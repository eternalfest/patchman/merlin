package merlin;

import etwin.ds.Nil;
import etwin.flash.MovieClip;
import hf.Case;
import hf.Hf;
import hf.mode.GameMode;
import hf.levels.ScriptEngine;
import merlin.impl.Host;
import merlin.value.MerlinValue;

class ActionContext {
  private var hf: Hf;
  private var game: GameMode;
  private var host: Host;

  private var evalExpr: ScriptExpr -> MerlinValue;
  private var _addNode: ScriptNode -> Void;
  private var node: ScriptNode;

  @:allow(merlin.impl.Engine)
  private function new(host: Host, evalExpr: ScriptExpr -> MerlinValue, addNode: ScriptNode -> Void, node: ScriptNode): Void {
    this.hf = host.game.root;
    this.game = host.game;
    this.host = host;
    this.evalExpr = evalExpr;
    this._addNode = addNode;
    this.node = node;
  }
  public inline function isOnAttach(): Bool {
    return this.host.fl_onAttach;
  }

  public inline function isBirth(): Bool {
    return this.host.fl_birth;
  }

  public inline function isDeath(): Bool {
    return this.host.fl_death;
  }

  public inline function isSafe(): Bool {
    return this.host.fl_safe;
  }

  public inline function getFirstTorch(): Bool {
    return this.host.fl_firstTorch;
  }

  public inline function setFirstTorch(value: Bool): Void {
    this.host.fl_firstTorch = value;
  }

  public inline function setRedraw(value: Bool): Void {
    this.host.fl_redraw = value;
  }

  public inline function getElevatorOpen(): Bool {
    return this.host.fl_elevatorOpen;
  }

  public inline function setElevatorOpen(value: Bool): Void {
    this.host.fl_elevatorOpen = value;
  }

  public inline function getBossDoorTimer(): Float {
    return this.host.bossDoorTimer;
  }

  public inline function setBossDoorTimer(value: Float): Void {
    this.host.bossDoorTimer = value;
  }

  public inline function getCycle(): Float {
    return this.host.cycle;
  }

  public inline function getGame(): GameMode {
    return this.host.game;
  }

  public inline function getHf(): Hf {
    return this.hf;
  }

  public inline function getRecentExp(): Array<ScriptExp> {
    return this.host.recentExp;
  }

  public inline function getEntries(): Array<Case> {
    return this.host.entries;
  }

  public inline function killById(id: Int): Void {
    return this.host.killById(id);
  }

  public inline function playById(id: Int): Void {
    return this.host.playById(id);
  }

  public inline function incBads(): Void {
    this.host.bads += 1;
  }

  public inline function registerMc(sid: Null<Int>, mc: MovieClip): Void {
    // TODO: Avoid using `untyped`
    var scriptMc: ScriptMc = {sid: sid, mc: untyped mc};
    this.host.mcList.push(scriptMc);
  }

  public function getMc(sid: Int): Null<MovieClip> {
    for (scriptMc in this.host.mcList) {
      if (scriptMc.sid == sid) {
        return scriptMc.mc;
      }
    }
    return null;
  }

  public inline function hasKey(kid: Int): Bool {
    return this.game.hasKey(kid);
  }

  public inline function keyUsed(kid: Int): Void {
    return this.game.fxMan.keyUsed(kid);
  }

  public inline function keyRequired(kid: Int): Void {
    return this.game.fxMan.keyRequired(kid);
  }

  public inline function addNode(node: ScriptNode): Void {
    return this._addNode(node);
  }

  public function getBool(name: String): Bool {
    return toBool(this.getArg(name));
  }

  public function getOptBool(name: String): Nil<Bool> {
    return this.getOptArg(name).map(val => toBool(val));
  }

  public function getBoolOr(name: String, defaultVal: Bool): Bool {
    return this.getOptBool(name).or(defaultVal);
  }

  public function getInt(name: String): Int {
    return toInt(this.getArg(name));
  }

  public function getOptInt(name: String): Nil<Int> {
    return this.getOptArg(name).map(val => toInt(val));
  }

  public function getOptString(name: String): Nil<String> {
    return this.getOptArg(name).map(val => toStr(val));
  }

  public function getString(name: String): String {
    return toStr(this.getArg(name));
  }

  public function getFloat(name: String): Float {
    return toFloat(this.getArg(name));
  }

  public function getOptFloat(name: String): Nil<Float> {
    return this.getOptArg(name).map(val => toFloat(val));
  }

  public function getArg(name: String): MerlinValue {
    var value: Nil<MerlinValue> = this.getOptArg(name);
    return value.or(_ => throw new ScriptError("Missing required argument: " + name));
  }

  public function getOptArg(name: String): Nil<MerlinValue> {
    if (this.node.args.exists(name)) {
      var expr: ScriptExpr = this.node.args.get(name);
      return Nil.some(this.evalExpr(expr));
    } else {
      return Nil.none();
    }
  }

  public inline function setArg(name: String, value: MerlinValue): Void {
    this.node.args.set(name, value);
  }

  public inline function getRuns(): Float {
    return this.node.runs;
  }

  public inline function setRuns(value: Float): Void {
    this.node.runs = value;
  }

  public function getArgNames(): Array<String> {
    return [for (name in this.node.args.keys()) name];
  }

  public inline function removeSelf(): Void {
    this.node.runs = 0;
  }

  private static inline function toBool(val: MerlinValue): Bool {
    if (val.isBool()) {
      return val.unwrapBool();
    }
    // TODO: Warn
    return val.isNull() ? false : val.unwrapFloat() != 0;
  }

  private static inline function toInt(val: MerlinValue): Int {
    return Std.int(val.unwrapFloat());
  }

  private static inline function toFloat(val: MerlinValue): Float {
    return val.unwrapFloat();
  }

  private static inline function toStr(val: MerlinValue): String {
    return val.unwrapString();
  }
}
