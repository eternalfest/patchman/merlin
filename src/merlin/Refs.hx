package merlin;

import merlin.value.MerlinFunction;
import merlin.value.MerlinFunction;
import merlin.refs.FrozenFactory;
import merlin.refs.GlobalFactory;
import merlin.refs.LevelFactory;
import merlin.refs.DynFactory;
import etwin.Obfu;

/**
  The operators, functions and variables provided by Merlin.
**/
class Refs {

  /**
    The `global` object, for storing persistent state across the whole run.
  **/
  public static var GLOBAL: IRefFactory = new GlobalFactory(Obfu.raw("global"));
  
  /**
    The `level` object, for storing persistent state tied to a specific level.

    This is the default scope; accesses to non-existant variables are resolved on this object.
  **/
  public static var LEVEL: IRefFactory = new LevelFactory(Obfu.raw("level"));

  /**
    The `dyn` object, for storing transient state that is cleared upon exiting the level.

    Unlike `level` and `global`, it is case-insensitive, such that `dyn.foo` and `dyn.FOO` always
    refer to the same value.

    Some fields have special meaning:
    
    - `dyn.AQUA`: if set, activates "aqua mode".
    - `dyn.BAD_BOMB_TRIGGER`: if set, bombs and mines from enemies will activate `exp` triggers.
    - `dyn.ICE`: if set, makes the level slippery.
    - `dyn.SAW_SPEED`: can be used to control the speed of saws in the level (default speed: `3`).
    - `dyn.SPEAR_SKIN`: selects the skin of spikes (`1`: normal; `2`: alternate).
  **/
  public static var DYN: IRefFactory = new DynFactory(Obfu.raw("dyn"));

  /**
    The `+` binary operator. Adds numbers or concatenates strings.
  **/
  public static var ADD: IRefFactory = new FrozenFactory(Obfu.raw("+"), MerlinFunction.fixedArity(Funcs.add));

  /**
    The `&&` binary operator. Computes the logical AND of two booleans, without short-circuiting.
  **/
  public static var AND: IRefFactory = new FrozenFactory(Obfu.raw("&&"), MerlinFunction.fixedArity(Funcs.and));

  /**
    The `&` binary operator. Computes the bitwise AND of two integers.
  **/
  public static var BIT_AND: IRefFactory = new FrozenFactory(Obfu.raw("&"), MerlinFunction.fixedArity(Funcs.bitAnd));
  
  /**
    The `~` unary operator. Computes the bitwise NOT of an integer.
  **/
  public static var BIT_NOT: IRefFactory = new FrozenFactory(Obfu.raw("~"), MerlinFunction.fixedArity(Funcs.bitNot));

  // Note: use U+2223 (DIVIDES) instead of the normal |, as the latter causes rendering bugs.
  /**
    The `∣` binary operator. Computes the bitwise OR of two integers.
  **/
  public static var BIT_OR: IRefFactory = new FrozenFactory(Obfu.raw("|"), MerlinFunction.fixedArity(Funcs.bitOr));

  /**
    The `^` binary operator. Computes the bitwise XOR of two integers.
  **/
  public static var BIT_XOR: IRefFactory = new FrozenFactory(Obfu.raw("^"), MerlinFunction.fixedArity(Funcs.bitXor));

  /**
    The `/` binary operator. Divides two numbers.
  **/
  public static var DIV: IRefFactory = new FrozenFactory(Obfu.raw("/"), MerlinFunction.fixedArity(Funcs.div));

  /**
    The `==` binary operator. Tests two values for equality.
  **/
  public static var EQ: IRefFactory = new FrozenFactory(Obfu.raw("=="), MerlinFunction.fixedArity(Funcs.eq));

  /**
    The `>` binary operator. Compares two numbers.
  **/
  public static var GT: IRefFactory = new FrozenFactory(Obfu.raw(">"), MerlinFunction.fixedArity(Funcs.gt));

  /**
    The `>=` binary operator. Compares two numbers.
  **/
  public static var GTE: IRefFactory = new FrozenFactory(Obfu.raw(">="), MerlinFunction.fixedArity(Funcs.gte));

  /**
    The `<` binary operator. Compares two numbers.
  **/
  public static var LT: IRefFactory = new FrozenFactory(Obfu.raw("<"), MerlinFunction.fixedArity(Funcs.lt));

  /**
    The `>=` binary operator. Compares two numbers.
  **/
  public static var LTE: IRefFactory = new FrozenFactory(Obfu.raw("<="), MerlinFunction.fixedArity(Funcs.lte));

  /**
    The `max` function. Returns the maximum of all its arguments. If no arguments are provided, returns `-inf`.
  **/
  public static var MAX: IRefFactory = new FrozenFactory(Obfu.raw("max"), MerlinFunction.fromFunc(Funcs.max));

  /**
    The `min` function. Returns the minimum of all its arguments. If no arguments are provided, returns `+inf`.
  **/
  public static var MIN: IRefFactory = new FrozenFactory(Obfu.raw("min"), MerlinFunction.fromFunc(Funcs.min));

  /**
    The `*` binary operator. Multiplies two numbers.
  **/
  public static var MUL: IRefFactory = new FrozenFactory(Obfu.raw("*"), MerlinFunction.fixedArity(Funcs.mul));
  
  /**
    The `!=` binary operator. The logical negation of `==`.
  **/
  public static var NE: IRefFactory = new FrozenFactory(Obfu.raw("!="), MerlinFunction.fixedArity(Funcs.ne));

  /**
  The `!` unary operator. Negates a boolean.
  **/
  public static var NOT: IRefFactory = new FrozenFactory(Obfu.raw("!"), MerlinFunction.fixedArity(Funcs.not));
  
  /**
    The `&&` binary operator. Computes the logical OR of two booleans, without short-circuiting.
  **/
  public static var OR: IRefFactory = new FrozenFactory(Obfu.raw("||"), MerlinFunction.fixedArity(Funcs.or));
  
  /**
    The `%` binary operator. Computes the modulo of two numbers.
  **/
  public static var REM: IRefFactory = new FrozenFactory(Obfu.raw("%"), MerlinFunction.fixedArity(Funcs.rem));
  
  /**
    The `-` binary operator. Substracts two numbers.
  **/
  public static var SUB: IRefFactory = new FrozenFactory(Obfu.raw("-"), MerlinFunction.fixedArity(Funcs.sub));

  public static function get(): Array<IRefFactory> {
    return [
      GLOBAL, LEVEL, DYN,

      ADD,
      AND,
      BIT_AND,
      BIT_NOT,
      BIT_OR,
      BIT_XOR,
      DIV,
      EQ,
      GT,
      GTE,
      LT,
      LTE,
      MAX,
      MIN,
      MUL,
      NE,
      NOT,
      OR,
      REM,
      SUB,
    ];
  }
}
