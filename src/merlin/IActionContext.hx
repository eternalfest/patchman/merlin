package merlin;

import etwin.ds.Nil;
import merlin.value.MerlinValue;

// Kept for backwards compatibility only.
typedef IActionContext = ActionContext;
