package merlin;

interface IAction {
  var name(default, null): String;

  var isVerbose(default, null): Bool;

  function run(ctx: ActionContext): Bool;
}
