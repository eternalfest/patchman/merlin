package merlin;

import patchman.DebugConsole;
import etwin.ds.Set;

class Deprecated {
  private static var ids(default, never): Set<String> = new Set();

  public static function warn(id: String, message: String) {
    if (!ids.exists(id)) {
      DebugConsole.warn(message);
      ids.add(id);
    }
  }
}
