package merlin.value;

import merlin.proxys.Plain;
import patchman.Assert;
import etwin.ds.Nil;

/**
  Opaque class representing a script value known to be an object.
**/
abstract MerlinObject(IProxy) from IProxy to IProxy {
  private inline function new(value: IProxy): Void {
    this = value;
  }

  @:to
  public inline function toMerlinValue(): MerlinValue {
    return cast this;
  }

  /**
    Creates a new object using a custom proxy.
  **/
  public inline static function proxy(p: IProxy): MerlinObject {
    return new MerlinObject(p);
  }

  /**
    Creates a new plain object.
  **/
  public inline static function plain(): MerlinObject {
    return new MerlinObject(new Plain());
  }

  public inline function hasField(key: String): Bool {
    return this.has(key);
  }

  public inline function setField(key: String, value: MerlinValue): Void {
    this.set(key, value);
  }

  public inline function tryGetField(key: String): Nil<MerlinValue> {
    return this.get(key);
  }

  public inline function getField(key: String): MerlinValue {
    return this.get(key).or(_ => throw ScriptError.missingField(key));
  }

  public function getFieldOr(key: String, defaultValue: MerlinValue): MerlinValue {
    return this.get(key).or(defaultValue);
  }
}
