package merlin.value;

/**
  Opaque class representing a script value known to be a boolean.
**/
abstract MerlinBool(Bool) from Bool {
  public inline function new(value: Bool): Void {
    this = value;
  }

  @:to
  public inline function toMerlinValue(): MerlinValue {
    return cast this;
  }

  @:to
  public inline function toBool(): Bool {
    return this;
  }
}
