package merlin.value;

/**
  Opaque class representing a script value known to be a string.
**/
abstract MerlinString(String) from String {
  public inline function new(value: String): Void {
    this = value;
  }

  @:to
  public inline function toMerlinValue(): MerlinValue {
    return this;
  }

  @:to
  public inline function toString(): String {
    return this;
  }
}
