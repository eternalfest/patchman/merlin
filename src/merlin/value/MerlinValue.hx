package merlin.value;

import merlin.impl.TypeUtils;

/**
 Opaque class representing a script value.
**/
abstract MerlinValue(Dynamic) {
  @:from
  public static inline function fromFloat(val: Null<Float>): MerlinValue {
    return cast val;
  }

  @:from
  public static inline function fromBool(val: Null<Bool>): MerlinValue {
    return cast val;
  }

  @:from
  public static inline function fromString(val: Null<String>): MerlinValue {
    return cast val;
  }

  public inline function isBool(): Bool {
    return TypeUtils.isBool(this);
  }

  public inline function isFloat(): Bool {
    return TypeUtils.isFloat(this);
  }

  public inline function isNull(): Bool {
    return this == null;
  }

  public inline function isString(): Bool {
    return TypeUtils.isString(this);
  }

  public inline function isObject(): Bool {
    return Std.is(this, IProxy);
  }

  public inline function isFunction(): Bool {
    return Std.is(this, ICall);
  }

  public inline function unwrapBool(): MerlinBool {
    return TypeUtils.isBool(this) ? this : throw ScriptError.expected("a boolean", this);
  }

  public inline function unwrapFloat(): MerlinFloat {
    return TypeUtils.isFloat(this) ? this : throw ScriptError.expected("a number", this);
  }

  public inline function unwrapString(): MerlinString {
    return TypeUtils.isString(this) ? this : throw ScriptError.expected("a string", this);
  }

  public inline function unwrapNull(): MerlinNull {
    return (this == null) ? this : throw ScriptError.expected("null", this);
  }

  public inline function unwrapObject(): MerlinObject {
    return Std.is(this, IProxy) ? this : throw ScriptError.expected("a Merlin object", this);
  }

  public inline function unwrapFunction(): MerlinFunction {
    return Std.is(this, ICall) ? this : throw ScriptError.expected("a Merlin function", this);
  }

  /**
   Tests if `raw` is a `MerlinValue`
   */
  public static inline function test(raw: Dynamic): Bool {
    return TypeUtils.isMerlinPrimitive(raw) || Std.is(raw, IProxy) || Std.is(raw, ICall);
  }
}
