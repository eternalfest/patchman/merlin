package merlin.value;

/**
  Opaque class representing a script value known to be null.
**/
abstract MerlinNull(Null<Dynamic>) {
  public static inline var NULL: MerlinNull = new MerlinNull();

  public inline function new(): Void {
    this = null;
  }

  @:to
  public inline function toMerlinValue(): MerlinValue {
    return cast this;
  }
}
