package merlin.value;

import etwin.Error;
import patchman.Assert;

import merlin.proxys.Callable;

/**
  Opaque class representing a script value known to be a function.
**/
abstract MerlinFunction(ICall) from ICall to ICall {
  private inline function new(value: ICall): Void {
    this = value;
  }

  @:to
  public inline function toMerlinValue(): MerlinValue {
    return cast this;
  }

  /**
    Creates a new function using a custom callable.
  **/
  public inline static function fromCallable(callable: ICall): MerlinFunction {
    return new MerlinFunction(callable);
  }

  /**
    Calls the function with the given arguments.
  **/
  public inline function call(args: Array<MerlinValue>): MerlinValue {
    return this.call(args);
  }

  /**
    Creates a new function from a closure.
  **/
  public inline static function fromFunc(func: Array<MerlinValue> -> MerlinValue): MerlinFunction {
    return new MerlinFunction(new Callable(func));
  }

  /**
    Creates a new function from a closure taking a fixed number of `MerlinValue` arguments.

    The resulting function will throw an error at runtime if the number of provided arguments doesn't match.
  **/
  public static macro function fixedArity(func: haxe.macro.Expr): haxe.macro.Expr.ExprOf<MerlinFunction> {
    return merlin.macro.MerlinFunctionImpl.fixedArity(func);
  }
}
