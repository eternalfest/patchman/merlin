package merlin.value;

/**
  Opaque class representing a script value known to be a float.
**/
abstract MerlinFloat(Float) from Float {
  public inline function new(value: Float): Void {
    this = value;
  }

  @:to
  public inline function toMerlinValue(): MerlinValue {
    return this;
  }

  @:to
  public inline function toFloat(): Float {
    return this;
  }
}
