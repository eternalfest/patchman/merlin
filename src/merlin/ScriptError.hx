package merlin;

import etwin.Error;
import merlin.value.MerlinValue;

class ScriptError extends Error {

  public function new(msg: String): Void {
    super(msg);
  }

  public static function expected(type: String, got: Dynamic): ScriptError {
    return new ScriptError('expected $type, got $got');
  }

  public static function missingField(field: String): ScriptError {
    return new ScriptError('accessing missing field: $field');
  }
}
