package merlin;

class ScriptPrinter {
  public static inline var INDENT: String = "  ";

  public static function stringify(script: Script): String {
    var chunks: Array<String> = [];
    writeScript(chunks, script);
    return chunks.join("");
  }

  private static function writeScript(chunks: Array<String>, script: Script): Void {
    chunks.push("SCRIPT:\n");
    writeNodeList(chunks, 0, script.nodes);
  }

  private static function writeNodeList(chunks: Array<String>, depth: Int, nodes: Array<ScriptNode>): Void {
    var first: Bool = true;
    for (node in nodes) {
      if (first) {
        first = false;
      } else {
        chunks.push("\n");
      }
      writeNode(chunks, depth, node);
    }
  }

  private static function writeNode(chunks: Array<String>, depth: Int, node: ScriptNode): Void {
    writeIndent(chunks, depth);
    chunks.push(node.name + "(");
    for (argName in node.args.keys()) {
      chunks.push(argName + "=..., "); // TODO: display arg. values
    }
    chunks.push(")");

    switch (node.onSuccess) {
      case Nothing: null;
      case Detach: chunks.push(" detach");
      case Copy: chunks.push(" copy");
    }

    if (node.runs == Math.POSITIVE_INFINITY) {
      chunks.push(" repeat");
    } else if (node.runs != 1) {
      chunks.push(" repeat=" + node.runs);
    }

    if (!node.okValue) {
      chunks.push(" invert");
    }

    if (node.key != null) {
      chunks.push(" key=" + node.key);
    }

    switch (node.clear) {
      case None: null;
      case End: chunks.push(" clear=\"end\"");
      case Exit: chunks.push(" clear=\"exit\"");
    }

    if (node.children == null || node.children.length == 0) {
      chunks.push(";");
      return;
    }
    chunks.push(" {\n");
    writeNodeList(chunks, depth + 1, node.children);
    chunks.push("\n");
    writeIndent(chunks, depth);
    chunks.push("}");
  }

  private static function writeIndent(chunks: Array<String>, depth: Int): Void {
    for (_ in 0...depth) {
      chunks.push(INDENT);
    }
  }
}
