package merlin;

import etwin.Error;
import merlin.refs.DynFactory;
import merlin.refs.GlobalFactory;
import merlin.refs.LevelFactory;

/**
  User configuration
**/
@:build(patchman.Build.di())
class MerlinConfig {
  @:allow(merlin)
  private var actions: Map<String, IAction>;
  @:allow(merlin)
  private var refs: Map<String, IRefFactory>;

  public function new(
    actions: Map<String, IAction>,
    refs: Map<String, IRefFactory>
  ): Void {
    this.actions = actions;
    this.refs = refs;
  }

  public static function empty(): MerlinConfig {
    return new MerlinConfig(
      new Map(),
      new Map()
    );
  }

  public static function base() {
    var result: MerlinConfig = new MerlinConfig(
      Actions.get(),
      new Map()
    );
    result.addRefs(Refs.get());
    return result;
  }

  public function addActions(actions: Array<IAction>): Bool {
    var result: Bool = false;
    for (action in actions) {
      result = this.addAction(action) || result;
    }
    return result;
  }

  public function addAction(action: IAction): Bool {
    var old: Null<IAction> = this.actions.get(action.name);
    if (old != null) {
      if (old == action) {
        // We already have this action
        return false;
      } else {
        // Naming conflict
        throw new Error("ActionNameConflict: " + action.name);
      }
    } else {
      this.actions.set(action.name, action);
      return true;
    }
  }

  public function addRefs(refs: Array<IRefFactory>): Bool {
    var result: Bool = false;
    for (ref in refs) {
      result = this.addRef(ref) || result;
    }
    return result;
  }

  public function addRef(ref: IRefFactory): Bool {
    var old: Null<IRefFactory> = this.refs.get(ref.name);
    if (old != null) {
      if (old == ref) {
        // We already have this ref
        return false;
      } else {
        // Naming conflict
        throw new Error("RefNameConflict: " + ref.name);
      }
    } else {
      this.refs.set(ref.name, ref);
      return true;
    }
  }

  public function add(config: MerlinConfig): Bool {
    var result: Bool = false;
    for (key in config.actions.keys()) {
      result = this.addAction(config.actions.get(key)) || result;
    }
    for (key in config.refs.keys()) {
      result = this.addRef(config.refs.get(key)) || result;
    }
    return result;
  }

  public function clone(): MerlinConfig {
    var result: MerlinConfig = MerlinConfig.empty();
    result.add(this);
    return result;
  }

  @:diFactory
  public static function fromContents(
    actions: Array<IAction>,
    refs: Array<IRefFactory>,
    configs: Array<MerlinConfig>
  ): MerlinConfig {
    var result: MerlinConfig = MerlinConfig.base();
    result.addActions(actions);
    result.addRefs(refs);
    for (config in configs) {
      result.add(config);
    }
    return result;
  }
}
