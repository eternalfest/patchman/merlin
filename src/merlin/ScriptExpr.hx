package merlin;

import merlin.value.MerlinBool;
import merlin.value.MerlinFloat;
import merlin.value.MerlinString;
import merlin.value.MerlinValue;

/**
  Runtime representation of a script expression.
  
  A script expression represents a lazy computation. It is stored as an array
  of operations to evaluate on a stack machine.

  Can be one of two types:

  - `MerlinValue`: Evaluate directly to a value.  
    Note that only `Bool`s, `Float`s and `String`s are used in
    compiled scripts.
  - `Array<ScriptOp>`: Evaluate to the computation of this complex expression.  
    `ScriptOp` is one of: `null`, `String`, `Float`, `Bool`, `ScriptOpObject`
    (`MerlinObject`s or `MerlinFunction`s are forbidden here).
**/
abstract ScriptExpr(Dynamic) {
  public static inline function fromRaw(raw: Dynamic): ScriptExpr {
    return cast raw;
  }

  public inline function asRaw(): Dynamic {
    return this;
  }

  @:from
  public static inline function fromFloat(value: Null<Float>): ScriptExpr {
    return cast new MerlinFloat(value);
  }

  @:from
  public static inline function fromBool(value: Bool): ScriptExpr {
    return cast new MerlinBool(value);
  }

  @:from
  public static inline function fromString(value: String): ScriptExpr {
    return cast new MerlinString(value);
  }

  @:from
  public static inline function fromValue(value: MerlinValue): ScriptExpr {
    return cast value;
  }
}
