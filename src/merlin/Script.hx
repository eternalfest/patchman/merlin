package merlin;

import merlin.value.MerlinValue;

class Script {
  /**
   * Root nodes.
   */
  public var nodes(default, null): Array<ScriptNode>;

  public function new(?nodes: Array<ScriptNode>): Void {
    this.nodes = nodes != null ? nodes : [];
  }

  public function traverse(fn: ScriptNode -> Void): Void {
    for (node in this.nodes) {
      node.traverse(fn);
    }
  }
}
