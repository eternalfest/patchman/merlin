package merlin.actions;

import etwin.Obfu;

class KillPt implements IAction {
  public var name(default, null): String = Obfu.raw("killPt");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    ctx.getGame().killPointer();
    return false;
  }
}
