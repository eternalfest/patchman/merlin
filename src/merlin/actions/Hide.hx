package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;

class Hide implements IAction {
  public var name(default, null): String = Obfu.raw("hide");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var tiles: Bool = ctx.getBoolOr(Obfu.raw("tiles"), false);
    var borders: Bool = ctx.getBoolOr(Obfu.raw("borders"), false);

    game.world.view.fl_hideTiles = tiles;
    game.world.view.fl_hideBorders = borders;
    game.world.view.detach();
    game.world.view.attach();
    game.world.view.moveToPreviousPos();

    return false;
  }
}
