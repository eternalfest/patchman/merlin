package merlin.actions;

import etwin.ds.Nil;
import hf.entity.Bad in HfBad;
import hf.Hf;
import hf.mode.GameMode;
import etwin.Obfu;

class Bad implements IAction {
  public var name(default, null): String = Obfu.raw("bad");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    var i: Int = ctx.getInt(Obfu.raw("i"));
    var sys: Bool = ctx.getBoolOr(Obfu.raw("sys"), false);
    var sid: Nil<Int> = ctx.getOptInt(Obfu.raw("sid"));

    if (!ctx.isSafe()) {
      var CASE_WIDTH: Float = hf.Data.CASE_WIDTH;
      var BAD_CLEAR: Int = hf.Data.BAD_CLEAR;
      var rx: Float = hf.Entity.x_ctr(game.flipCoordCase(x)) - CASE_WIDTH * 0.5;
      var ry: Float = hf.Entity.y_ctr(y - 1);
      var bad: HfBad = game.attachBad(i, rx, ry);
      if ((bad.types & BAD_CLEAR) != 0) {
        if (sys && game.world.isVisited()) {
          bad.destroy();
          game.badCount--;
        }
        ctx.incBads();
        game.fl_clear = false;
      }
      sid.map(sid => {
        ctx.killById(sid);
        bad.scriptId = sid;
      });
    }

    return false;
  }
}
