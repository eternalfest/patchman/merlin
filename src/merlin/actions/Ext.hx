package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;

class Ext implements IAction {
  public var name(default, null): String = Obfu.raw("ext");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    if (game.canAddItem() && !ctx.isSafe()) {
      game.statsMan.attachExtend();
    }

    return false;
  }
}
