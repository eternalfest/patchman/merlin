package merlin.actions;

import etwin.Obfu;

class Attach implements IAction {
  public var name(default, null): String = Obfu.raw("attach");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    return ctx.isOnAttach();
  }
}
