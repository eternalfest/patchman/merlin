package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;

class HideBorders implements IAction {
  public var name(default, null): String = Obfu.raw("hideBorders");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    game.world.view.fl_hideTiles = true;
    game.world.view.detach();
    game.world.view.attach();
    game.world.view.moveToPreviousPos();

    return false;
  }
}
