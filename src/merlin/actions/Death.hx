package merlin.actions;

import etwin.Obfu;

class Death implements IAction {
  public var name(default, null): String = Obfu.raw("death");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    return ctx.isDeath();
  }
}
