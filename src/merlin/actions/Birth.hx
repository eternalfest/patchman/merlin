package merlin.actions;

import etwin.Obfu;

class Birth implements IAction {
  public var name(default, null): String = Obfu.raw("birth");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    return ctx.isBirth();
  }
}
