package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;

class OpenPortal implements IAction {
  public var name(default, null): String = Obfu.raw("openPortal");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    var pid: Int = ctx.getInt(Obfu.raw("pid"));

    game.openPortal(x, y, pid);

    return false;
  }
}
