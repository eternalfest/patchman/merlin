package merlin.actions;

import etwin.Obfu;

class Do implements IAction {
  public var name(default, null): String = Obfu.raw("do");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    return true;
  }
}
