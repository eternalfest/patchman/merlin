package merlin.actions;

import etwin.Obfu;

class KillMsg implements IAction {
  public var name(default, null): String = Obfu.raw("killMsg");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    ctx.getGame().killPop();
    return false;
  }
}
