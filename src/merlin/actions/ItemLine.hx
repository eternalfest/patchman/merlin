package merlin.actions;

import etwin.ds.Nil;
import hf.mode.GameMode;
import etwin.Obfu;

class ItemLine implements IAction {
  public var name(default, null): String = Obfu.raw("itemLine");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var x1: Int = ctx.getInt(Obfu.raw("x1"));
    var x2: Int = ctx.getInt(Obfu.raw("x2"));
    var y: Int = ctx.getInt(Obfu.raw("y"));
    var i: Int = ctx.getInt(Obfu.raw("i"));
    var si: Nil<Int> = ctx.getOptInt(Obfu.raw("si"));
    var t: Int = ctx.getInt(Obfu.raw("t"));

    var cycle: Float = ctx.getCycle();

    var idx: Int = 0;
    while (true) {
      var scoreArgs: Map<String, ScriptExpr> = [
        Obfu.raw("i") => i,
        Obfu.raw("x") => x1,
        Obfu.raw("y") => y,
        Obfu.raw("inf") => true
      ];
      si.map(si => scoreArgs.set(Obfu.raw("si"), si));
      var scoreNode = new ScriptNode(Actions.SCORE.name, scoreArgs, []);

      var timerArgs: Map<String, ScriptExpr> = [
        Obfu.raw("t") => cycle + idx * t
      ];
      var timerNode = new ScriptNode(Actions.TIMER.name, timerArgs, [scoreNode]);

      ctx.addNode(timerNode);
      if (x1 == x2) {
        break;
      }
      if (x1 < x2) {
        x1++;
      }
      if (x1 > x2) {
        x1--;
      }
      idx++;
    }

    return false;
  }
}
