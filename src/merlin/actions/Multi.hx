package merlin.actions;

import etwin.Obfu;

class Multi implements IAction {
  public var name(default, null): String = Obfu.raw("multi");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    return ctx.getGame().getPlayerList().length > 1;
  }
}
