package merlin.actions;

import patchman.DebugConsole;
import etwin.Obfu;

class Exec implements IAction {
  public var name(default, null): String = Obfu.raw("exec");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    // Evaluate expression and discard result
    // (We only care about side effects such as assignations)
    ctx.getArg(Obfu.raw("expr"));
    return false;
  }
}
