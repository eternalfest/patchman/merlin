package merlin.actions;

import hf.entity.Player;
import hf.Hf;
import hf.mode.GameMode;
import etwin.Obfu;

class Pointer implements IAction {
  public var name(default, null): String = Obfu.raw("pointer");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    x = game.flipCoordCase(x);

    var PLAYER: Int = hf.Data.PLAYER;
    var p: Player = cast game.getOne(PLAYER);

    game.attachPointer(x, y, p.cx, p.cy);

    return false;
  }
}
