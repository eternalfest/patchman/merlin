package merlin.actions;

import hf.Hf;
import hf.mode.GameMode;
import etwin.Obfu;

class Add implements IAction {
  public var name(default, null): String = Obfu.raw("add");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var x1: Int = ctx.getInt(Obfu.raw("x1"));
    var y1: Int = ctx.getInt(Obfu.raw("y1"));
    var x2: Int = ctx.getInt(Obfu.raw("x2"));
    var y2: Int = ctx.getInt(Obfu.raw("y2"));
    var type: Int = ctx.getOptInt(Obfu.raw("type")).or(0);

    x1 = Std.int(game.flipCoordCase(x1));
    x2 = Std.int(game.flipCoordCase(x2));

    var GROUND: Int = hf.Data.GROUND;

    if (type > 0) {
      type = -type;
    } else {
      type = GROUND;
    }

    while (true) {
      if (!(x1 != x2 || y1 != y2)) {
        break;
      }
      game.world.forceCase(x1, y1, type);
      if (x1 < x2) {
        x1++;
      }
      if (x1 > x2) {
        x1--;
      }
      if (y1 < y2) {
        y1++;
      }
      if (y1 > y2) {
        y1--;
      }
    }
    game.world.forceCase(x1, y1, type);
    ctx.setRedraw(true);
    return false;
  }
}
