package merlin.actions;

import etwin.flash.DynamicMovieClip;
import hf.Hf;
import hf.mode.GameMode;
import patchman.DebugConsole;
import etwin.ds.Nil;
import etwin.Obfu;

/*
  Changes from original `$e_mc` event:
    - `x/y` and `xr/yr` can be mixed, e.g. `x=..., yr=...`;
    - the `flip` flag can be used to mirror the sprite horizontally;
    - the torch halo get correctly placed if the sprite is
      mirrored (due to `flip`, or to the mirror mode)
*/
class Mc implements IAction {
  public var name(default, null): String = Obfu.raw("mc");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var x: Nil<Float> = ctx.getOptFloat(Obfu.raw("x"));
    var y: Nil<Float> = ctx.getOptFloat(Obfu.raw("y"));
    var xr: Nil<Float> = ctx.getOptFloat(Obfu.raw("xr"));
    var yr: Nil<Float> = ctx.getOptFloat(Obfu.raw("yr"));
    var sid: Null<Int> = ctx.getOptInt(Obfu.raw("sid")).toNullable();
    var back: Bool = ctx.getBoolOr(Obfu.raw("back"), false);
    var n: String = ctx.getString(Obfu.raw("n"));
    var p: Bool = ctx.getBoolOr(Obfu.raw("p"), false);

    var flip: Bool = ctx.getBoolOr(Obfu.raw("flip"), false); // extension

    var CASE_WIDTH: Float = hf.Data.CASE_WIDTH;
    var CASE_HEIGHT: Float = hf.Data.CASE_HEIGHT;

    ctx.killById(sid);

    var rx: Null<Float> = null;
    xr.map(xr => {
      rx = xr;
      if (x.isSome())
        DebugConsole.warn("BadMcParams: x and xr cannot be used at the same time");
    });
    x.map(x => rx = hf.Entity.x_ctr(x));
    if (rx == null) {
      DebugConsole.warn("BadMcParams: missing x or xr coordinate");
      return false;
    }

    var ry: Null<Float> = null;
    yr.map(yr => {
      ry = yr;
      if (y.isSome())
        DebugConsole.warn("BadMcParams: y and yr cannot be used at the same time");
    });
    y.map(y => ry = hf.Entity.y_ctr(y));
    if (rx == null) {
      DebugConsole.warn("BadMcParams: missing y or yr coordinate");
      return false;
    }

    if (game.fl_mirror)
      rx = game.flipCoordReal(rx) + CASE_WIDTH;

    var sprite: Null<DynamicMovieClip> = cast game.world.view.attachSprite(n, rx, ry, back);
    if (sprite == null) {
      DebugConsole.warn("BadMcParams: unknown sprite " + n);
      return false;
    }
    
    var flipped = game.fl_mirror != flip;
    if (flipped) sprite._xscale *= -1;

    if (p) {
      sprite.play();
    } else {
      sprite.stop();
    }

    if (sprite.sub != null)
      sprite.sub.stop();
    
    if (n == "$torch") {
      if (!ctx.getFirstTorch()) {
        game.clearExtraHoles();
      }
      // Place torch halo properly if the sprite is flipped.
      game.addHole(
        rx + CASE_WIDTH * (flipped ? -0.5 : 0.5),
        ry - CASE_HEIGHT * 0.5,
        180
      );
      game.updateDarkness();
      ctx.setFirstTorch(true);
    }
    ctx.registerMc(sid, sprite);

    return false;
  }
}
