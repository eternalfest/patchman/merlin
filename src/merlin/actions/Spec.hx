package merlin.actions;

import etwin.ds.Nil;
import hf.entity.item.SpecialItem;
import hf.Hf;
import hf.mode.GameMode;
import etwin.Obfu;
/*
  Changes from original `$e_spec` event:
    - `force` ca be used to bypass the checks preventing a
      special item from spawning.
*/
class Spec implements IAction {
  public var name(default, null): String = Obfu.raw("spec");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    var i: Int = ctx.getInt(Obfu.raw("i"));
    var si: Null<Int> = ctx.getOptInt(Obfu.raw("si")).toNullable();
    var inf: Bool = ctx.getBoolOr(Obfu.raw("inf"), false);
    var sid: Nil<Int> = ctx.getOptInt(Obfu.raw("sid"));

    var force: Bool = ctx.getBoolOr(Obfu.raw("force"), false);

    x = game.flipCoordReal(hf.Entity.x_ctr(x));
    y = hf.Entity.y_ctr(y);

    if (force || game.canAddItem() && !ctx.isSafe()) {
      var item: SpecialItem = hf.entity.item.SpecialItem.attach(game, x, y, i, si);
      if (inf) {
        item.setLifeTimer(-1);
      }
      sid.map(sid => {
        ctx.killById(sid);
        item.scriptId = sid;
      });
    }

    return false;
  }
}
