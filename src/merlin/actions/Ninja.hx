package merlin.actions;

import etwin.Obfu;

class Ninja implements IAction {
  public var name(default, null): String = Obfu.raw("ninja");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    return ctx.getGame().fl_ninja;
  }
}
