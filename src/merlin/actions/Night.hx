package merlin.actions;

import etwin.Obfu;

class Night implements IAction {
  public var name(default, null): String = Obfu.raw("night");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    return ctx.getGame().fl_nightmare;
  }
}
