package merlin.actions;

import hf.Hf;
import hf.mode.GameMode;
import etwin.Obfu;

class Exp implements IAction {
  public var name(default, null): String = Obfu.raw("exp");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    var rx: Float = game.flipCoordReal(hf.Entity.x_ctr(x));
    var ry: Float = hf.Entity.y_ctr(y);

    for (exp in ctx.getRecentExp()) {
      var distSquare = Math.pow(rx - exp.x, 2) + Math.pow(ry - exp.y, 2);
      if (distSquare <= Math.pow(exp.r, 2) && Math.sqrt(distSquare) <= exp.r) {
        return true;
      }
    }
    return false;
  }
}
