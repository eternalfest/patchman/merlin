package merlin.actions;

import etwin.Obfu;

class Goto implements IAction {
  public var name(default, null): String = Obfu.raw("goto");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var id: Int = ctx.getInt(Obfu.raw("id"));
    ctx.getGame().forcedGoto(id);
    return false;
  }
}
