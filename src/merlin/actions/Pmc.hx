package merlin.actions;

import etwin.ds.Nil;
import etwin.Obfu;

class Pmc implements IAction {
  public var name(default, null): String = Obfu.raw("pmc");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var sid: Nil<Int> = ctx.getOptInt(Obfu.raw("sid"));
    sid.map(sid => {ctx.playById(sid);});
    return false;
  }
}
