package merlin.actions;

import etwin.flash.MovieClip;
import hf.entity.Animator.Anim;
import hf.entity.bad.Jumper;
import hf.Hf;
import hf.mode.Adventure;
import hf.mode.GameMode;
import merlin.IAction;
import patchman.DebugConsole;
import etwin.Obfu;

class Ctrigger implements IAction {
  public var name(default, null): String = Obfu.raw("ctrigger");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var id: Int = ctx.getInt(Obfu.raw("id"));
    switch (id) {
      case 0: this.runWarpStart(ctx);
      case 1: this.runDelayHu(ctx);
      case 2: this.runDetachExit(ctx);
      case 3: this.runOpenElevator(ctx);
      case 4: this.runEndGame(ctx);
      case 5: this.runBossDoor(ctx);
      case 6: this.runAttachSoccerBall(ctx);
      case 7: this.runAnimLooser(ctx);
      case 8: this.runAnimVictor(ctx);
      case 9: this.runBossLaugh(ctx);
      case 10: this.runDisableJumpDown(ctx);
      case 11: this.runClearBads(ctx);
      case 12: this.runFireBall(ctx);
      case 13: this.runClearItems(ctx);
      case 14: this.runClearExtraHoles(ctx);
      case 15: this.runResetHurry(ctx);
    }
    return false;
  }

  public function runWarpStart(ctx: ActionContext): Void {
    var game: GameMode = ctx.getGame();
    var advGame: Adventure = cast game;
    advGame.fl_warpStart = true;
  }

  public function runDelayHu(ctx: ActionContext): Void {
    var tmod: Float = ctx.getHf().Timer.tmod;
    ctx.getGame().huTimer -= tmod * 0.5;
  }

  public function runDetachExit(ctx: ActionContext): Void {
    ctx.getGame().fxMan.detachExit();
  }

  public function runOpenElevator(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var SECOND: Int = hf.Data.SECOND;

    ctx.playById(101);
    ctx.setElevatorOpen(true);
    for (player in game.getPlayerList()) {
      player.lockControls(SECOND * 12.5);
      player.dx = 0.0;
    }
    game.huTimer = 0;
  }

  public function runEndGame(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var SECOND: Int = hf.Data.SECOND;

    if (ctx.getElevatorOpen()) {
      for (player in game.getPlayerList()) {
        player.hide();
        player.lockControls(99999);
      }
      game.huTimer = 0;
      var elevatorMc: Null<MovieClip> = ctx.getMc(101);
      if (elevatorMc != null) {
        // TODO: Type `elevatorMc`
        Reflect.setField(elevatorMc, "head", game.getPlayerList()[0].head);
      }
      ctx.playById(101);
      game.endModeTimer = SECOND * 14;
      ctx.setElevatorOpen(false);
    }
  }

  public function runBossDoor(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var BOSS: Int = hf.Data.BOSS;

    // TODO: Checked cast?
    var boss: Null<hf.entity.boss.Tuberculoz> = cast game.getOne(BOSS);
    if (boss.fl_defeated) {
      var tmod: Float = hf.Timer.tmod;
      var oldBossDoorTimer: Float = ctx.getBossDoorTimer();
      var bossDoorTimer: Float = oldBossDoorTimer - tmod;
      ctx.setBossDoorTimer(bossDoorTimer);
      if (bossDoorTimer <= 0) {
        game.destroyList(BOSS);
        game.world.view.destroy();
        game.forcedGoto(102);
      }
    }
  }

  public function runAttachSoccerBall(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var specialSlots = game.world.current.__dollar__specialSlots;

    var x = 0.0, y = 0.0;
    if (specialSlots.length != 0) {
      var slot = specialSlots[hf.Std.random(specialSlots.length)];
      x = game.flipCoordCase(slot.__dollar__x);
      y = slot.__dollar__y;
    }
    var slot = specialSlots[hf.Std.random(specialSlots.length)];
    var ball = hf.entity.bomb.player.SoccerBall.attach(game, hf.Entity.x_ctr(x), hf.Entity.y_ctr(y));

    ball.dx = (10 + hf.Std.random(10)) * (Std.random(2)*2 - 1);
    ball.dy = -hf.Std.random(5) - 5;
  }

  public function runAnimLooser(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var ANIM_PLAYER_WALK: Anim = hf.Data.ANIM_PLAYER_WALK;
    var ANIM_PLAYER_STOP_L: Anim = hf.Data.ANIM_PLAYER_STOP_L;

    for (player in game.getPlayerList()) {
      player.setBaseAnims(ANIM_PLAYER_WALK, ANIM_PLAYER_STOP_L);
    }
  }

  public function runAnimVictor(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var ANIM_PLAYER_WALK_V: Anim = hf.Data.ANIM_PLAYER_WALK_V;
    var ANIM_PLAYER_STOP_V: Anim = hf.Data.ANIM_PLAYER_STOP_V;

    for (player in game.getPlayerList()) {
      player.setBaseAnims(ANIM_PLAYER_WALK_V, ANIM_PLAYER_STOP_V);
    }
  }

  public function runBossLaugh(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var CHAN_BAD: Int = hf.Data.CHAN_BAD;

    game.soundMan.playSound("sound_boss_laugh", CHAN_BAD);
  }

  public function runDisableJumpDown(ctx: ActionContext): Void {
    var game: GameMode = ctx.getGame();

    for (bad in game.getBadList()) {
      var jumperBad: Jumper = cast bad;
      jumperBad.setJumpDown(null);
    }
  }

  public function runClearBads(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var CASE_HEIGHT: Int = hf.Data.CASE_HEIGHT;

    for (bad in game.getBadClearList()) {
      game.fxMan.attachFx(bad.x, bad.y - CASE_HEIGHT, "hammer_fx_pop");
      bad.destroy();
    }
  }

  public function runFireBall(ctx: ActionContext): Void {
    var game: GameMode = ctx.getGame();

    while (game.huState < 2) {
      var huMc: MovieClip = game.onHurryUp();
      if (game.huState < 2) {
        huMc.removeMovieClip();
      }
    }
  }

  public function runClearItems(ctx: ActionContext): Void {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var ITEM: Int = hf.Data.ITEM;
    var CASE_HEIGHT: Int = hf.Data.CASE_HEIGHT;

    for (item in game.getList(ITEM)) {
      game.fxMan.attachFx(item.x, item.y - CASE_HEIGHT, "hammer_fx_pop");
      item.destroy();
    }
  }

  public function runClearExtraHoles(ctx: ActionContext): Void {
    var game: GameMode = ctx.getGame();

    game.clearExtraHoles();
  }

  public function runResetHurry(ctx: ActionContext): Void {
    var game: GameMode = ctx.getGame();

    game.resetHurry();
  }
}
