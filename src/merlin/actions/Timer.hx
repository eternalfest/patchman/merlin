package merlin.actions;

import etwin.Obfu;

/*
  Changes from the original `$t_timer` trigger:
  - the `rel` flag makes `t` interpreted as a relative delay
    instead of an absolute cycle count.
*/
class Timer implements IAction {
  public var name(default, null): String = Obfu.raw("timer");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var t: Float = ctx.getFloat(Obfu.raw("t"));
    var base: Null<Float> = ctx.getOptFloat(Obfu.raw("base")).toNullable();
    var rel: Bool = ctx.getBoolOr(Obfu.raw("rel"), false);

    var cycle: Float = ctx.getCycle();

    if (rel) {
      if (base == null) {
        base = t;
        ctx.setArg(Obfu.raw("base"), base);
      }

      t += cycle;
      ctx.setArg(Obfu.raw("t"), t);

      ctx.setArg(Obfu.raw("rel"), false);
    }

    var result = cycle >= t;

    if (result) {
      var oldRuns: Float = ctx.getRuns();
      if (oldRuns > 1) {
        // Infinite repeat or more executions beside this one:
        // Rewrite itself for the next execution
        if (base == null) {
          base = t;
          ctx.setArg(Obfu.raw("base"), base);
        }
        ctx.setArg(Obfu.raw("t"), ctx.getCycle() + base);
      }
    }
    return result;
  }
}
