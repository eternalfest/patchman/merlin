package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;
import patchman.Ref;

class Enter implements IAction {
  public var name(default, null): String = Obfu.raw("enter");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    x = game.flipCoordCase(x);

    for (entry in ctx.getEntries()) {
      if (entry.cx == x && entry.cy == y) {
        return true;
      }
    }
    return false;
  }
}
