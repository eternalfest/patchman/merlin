package merlin.actions;

import etwin.ds.Nil;
import etwin.Obfu;

class Kill implements IAction {
  public var name(default, null): String = Obfu.raw("kill");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var sid: Nil<Int> = ctx.getOptInt(Obfu.raw("sid"));
    sid.map(sid => {ctx.killById(sid);});
    return false;
  }
}
