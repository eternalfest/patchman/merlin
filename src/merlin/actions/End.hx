package merlin.actions;

import etwin.Obfu;

class End implements IAction {
  public var name(default, null): String = Obfu.raw("end");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    return ctx.getGame().fl_clear && ctx.getCycle() > 10;
  }
}
