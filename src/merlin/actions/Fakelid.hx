package merlin.actions;

import etwin.ds.Nil;
import hf.mode.GameMode;
import etwin.Obfu;

class Fakelid implements IAction {
  public var name(default, null): String = Obfu.raw("fakelid");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var lid: Nil<Float> = ctx.getOptFloat(Obfu.raw("lid"));
    lid.map(lid => {
      if (!Math.isNaN(lid)) {
        // TODO: Allow float?
        game.fakeLevelId = Std.int(lid);
        game.gi.setLevel(lid);
        return false;
      }
    });

    game.fakeLevelId = null;
    game.gi.hideLevel();
    return false;
  }
}
