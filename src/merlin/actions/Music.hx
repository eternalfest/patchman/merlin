package merlin.actions;

import etwin.ds.Nil;
import hf.mode.GameMode;
import etwin.Obfu;

class Music implements IAction {
  public var name(default, null): String = Obfu.raw("music");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var id: Nil<Int> = ctx.getOptInt(Obfu.raw("id"));
    id.map(id => {game.playMusic(id);});

    return false;
  }
}
