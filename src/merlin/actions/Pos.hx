package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;

class Pos implements IAction {
  public var name(default, null): String = Obfu.raw("pos");
  public var isVerbose(default, null): Bool = true;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    var d: Float = ctx.getFloat(Obfu.raw("d"));
    x = game.flipCoordCase(x);

    for (player in game.getPlayerList()) {
      if (!player.fl_kill && !player.fl_destroy) {
        var dist: Float = player.distanceCase(x, y);
        if (dist <= d && !Math.isNaN(d)) {
          return true;
        }
      }
    }

    return false;
  }
}
