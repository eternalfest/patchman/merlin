package merlin.actions;

import etwin.ds.Nil;
import hf.entity.item.ScoreItem;
import hf.Hf;
import hf.mode.GameMode;
import etwin.Obfu;

class Score implements IAction {
  public var name(default, null): String = Obfu.raw("score");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var x: Float = ctx.getFloat(Obfu.raw("x"));
    var y: Float = ctx.getFloat(Obfu.raw("y"));
    var i: Int = ctx.getInt(Obfu.raw("i"));
    var si: Null<Int> = ctx.getOptInt(Obfu.raw("si")).toNullable();
    var inf: Bool = ctx.getBoolOr(Obfu.raw("inf"), false);
    var sid: Nil<Int> = ctx.getOptInt(Obfu.raw("sid"));

    x = game.flipCoordReal(hf.Entity.x_ctr(x));
    y = hf.Entity.y_ctr(y);

    var item: ScoreItem = hf.entity.item.ScoreItem.attach(game, x, y, i, si);
    if (inf) {
      item.setLifeTimer(-1);
    }
    sid.map(sid => {
      ctx.killById(sid);
      item.scriptId = sid;
    });

    return false;
  }
}
