package merlin.actions;

import hf.Hf;
import hf.mode.GameMode;
import patchman.DebugConsole;
import etwin.Obfu;

class Tuto implements IAction {
  public var name(default, null): String = Obfu.raw("tuto");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();

    var id: Int = ctx.getInt(Obfu.raw("id"));
    // In the original code, `id` may be missing. In this case, a warning is
    // emited and `txt` is the inner content of the XML tag.
    var txt: Null<String> = hf.Lang.get(id);
    if (txt != null) {
      game.attachPop("\n" + txt, true);
    } else {
      DebugConsole.warn("BadMsgId: " + id);
    }
    return false;
  }
}
