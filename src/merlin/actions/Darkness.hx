package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;

class Darkness implements IAction {
  public var name(default, null): String = Obfu.raw("darkness");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var v: Int = ctx.getInt(Obfu.raw("v"));

    game.forcedDarkness = cast v;
    game.updateDarkness();

    return false;
  }
}
