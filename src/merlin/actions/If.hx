package merlin.actions;

import patchman.DebugConsole;
import etwin.Obfu;

class If implements IAction {
  public var name(default, null): String = Obfu.raw("if");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var result: Bool = ctx.getBool(Obfu.raw("test"));
    return result;
  }
}
