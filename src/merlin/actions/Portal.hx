package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;

class Portal implements IAction {
  public var name(default, null): String = Obfu.raw("portal");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var pid: Int = ctx.getInt(Obfu.raw("pid"));

    if (game.fl_clear && ctx.getCycle() > 10) {
      game.usePortal(pid, null);
    }

    return false;
  }
}
