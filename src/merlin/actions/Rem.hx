package merlin.actions;

import hf.mode.GameMode;
import etwin.Obfu;

class Rem implements IAction {
  public var name(default, null): String = Obfu.raw("rem");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: ActionContext): Bool {
    var game: GameMode = ctx.getGame();

    var x1: Int = ctx.getInt(Obfu.raw("x1"));
    var y1: Int = ctx.getInt(Obfu.raw("y1"));
    var x2: Int = ctx.getInt(Obfu.raw("x2"));
    var y2: Int = ctx.getInt(Obfu.raw("y2"));

    x1 = Std.int(game.flipCoordCase(x1));
    x2 = Std.int(game.flipCoordCase(x2));

    while (true) {
      if (!(x1 != x2 || y1 != y2)) {
        break;
      }
      game.world.forceCase(x1, y1, 0);
      if (x1 < x2) {
        x1++;
      }
      if (x1 > x2) {
        x1--;
      }
      if (y1 < y2) {
        y1++;
      }
      if (y1 > y2) {
        y1--;
      }
    }
    game.world.forceCase(x1, y1, 0);
    ctx.setRedraw(true);
    return false;
  }
}
