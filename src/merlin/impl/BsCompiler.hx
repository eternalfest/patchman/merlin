package merlin.impl;

import merlin.impl.bs.Script in BsScript;
import merlin.impl.bs.ScriptExpr in BsScriptExpr;
import merlin.impl.bs.ScriptNode in BsScriptNode;
import merlin.ScriptNode;
import patchman.Assert;
import etwin.Obfu;

/**
  The `BsCompiler` class allows to convert a _BetterScript_ AST into a
  `Script` object.
**/
class BsCompiler {
  private static inline var EXEC: String = Obfu.raw("exec");
  private static inline var EXPR: String = Obfu.raw("expr");
  private static inline var IF: String = Obfu.raw("if");
  private static inline var TEST: String = Obfu.raw("test");

  public static function compile(ast: BsScript, allowDeprecated: Bool = false): Script {
    if (ast.version() != 1) {
      throw new ScriptError("UnexpectedScriptVersion: " + ast.version());
    }
    var nodes: Array<ScriptNode> = compileNodeList(ast.nodes(), allowDeprecated);
    return new Script(nodes);
  }

  private static function compileNodeList(astNodes: Array<BsScriptNode>, allowDeprecated: Bool = false): Array<ScriptNode> {
    var nodes: Array<ScriptNode> = [];
    for (astNode in astNodes) {
      nodes.push(compileNode(astNode, allowDeprecated));
    }
    return nodes;
  }

  private static function compileNode(astNode: BsScriptNode, allowDeprecated: Bool = false): ScriptNode {
    var name: String = normalizeName(astNode.name(), allowDeprecated);

    // TODO: Disable check in release mode
    if (name.substr(0, 2) == Obfu.raw("t_") || name.substr(0, 2) == Obfu.raw("e_")) {
      Deprecated.warn("prefixedName", "Deprecated use of prefix in action name: " + name);
    }

    var args: Map<String, ScriptExpr> = new Map();
    var children: Array<ScriptNode> = astNode.children() != null ? compileNodeList(astNode.children(), allowDeprecated) : [];
    var runs: Float = astNode.repeat() == null ? 1 : (astNode.repeat() < 0 || astNode.repeat() == true) ? Math.POSITIVE_INFINITY : astNode.repeat();
    var okValue: Bool = astNode.invert() == null;
    var key: Null<Int> = astNode.key();
    var detach: Bool = astNode.detach() != null && astNode.detach();
    var copy: Bool = astNode.copy() != null && astNode.copy();
    var onSuccess: NodeOnSuccess = detach ? Detach : (copy ? Copy : Nothing);

    var clear: NodeClearEvent = if (astNode.clear() == null) {
      None;
    } else if (astNode.clear() == Obfu.raw("end")) {
      End;
    } else if (astNode.clear() == Obfu.raw("exit")) {
      Exit;
    } else {
      throw new ScriptError("ScriptCompile: Unknown clear type " + astNode.clear());
    };

    var params: Null<Dynamic> = astNode.params();
    if (params != null) {
      if (name == EXEC) {
        args.set(EXPR, ScriptExpr.fromRaw(params));
      } else if (name == IF && !Reflect.hasField(params, TEST)) {
        args.set(TEST, ScriptExpr.fromRaw(params));
      } else {
        var paramNames: Array<String> = Reflect.fields(params);
        for (paramName in paramNames) {
          var paramExpr: BsScriptExpr = Reflect.field(params, paramName);
          // `ScriptExpr` uses an inner representation compatible with `BsScriptExpr`.
          args.set(paramName, ScriptExpr.fromRaw(paramExpr));
        }
      }
    }

    var node: ScriptNode = new ScriptNode(name, args, children, runs, okValue, key, onSuccess, clear);
    return normalizeNode(node);
  }

  private static function normalizeName(str: String, allowDeprecated: Bool = false): String {
    return Actions.normalizeActionName(str, allowDeprecated);
  }

  private static function normalizeNode(node: ScriptNode): ScriptNode {
    if (node.name == Obfu.raw("ifVar")) {
      return normalizeIfVarNode(node);
    } else if (node.name == Obfu.raw("e_setVar")) {
      return normalizeSetVarNode(node);
    } else {
      return node;
    }
  }

  private static function normalizeIfVarNode(node: ScriptNode): ScriptNode {
    Deprecated.warn("ifVar", "Deprecated `ifVar` action, use `if`");
    var equalsExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("equals"));
    var gtExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("gt"));
    var ltExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("lt"));
    var varExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("var"));
    var gExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("g"));

    Assert.debug(varExpr != null);

    var rightExpr: Null<ScriptExpr> = null;
    var binOp: Null<String> = null;

    if (equalsExpr != null) {
      rightExpr = equalsExpr;
      binOp = "==";
    } else if (gtExpr != null) {
      rightExpr = gtExpr;
      binOp = ">";
    } else if (ltExpr != null) {
      rightExpr = ltExpr;
      binOp = "<";
    }

    var testExpr: ScriptExpr;
    if (binOp == null) {
      // Always false if there is no operator / right side
      testExpr = ScriptExpr.fromBool(false);
    } else {
      Assert.debug(rightExpr != null && !TypeUtils.isArray(rightExpr));
      Assert.debug(Std.is(varExpr.asRaw(), String));
      var varPath: String = varExpr.asRaw();
      if (TypeUtils.isBool(gExpr) && gExpr.asRaw() || TypeUtils.isFloat(gExpr) && gExpr.asRaw() != 0) {
        varPath = Obfu.raw("global") + "." + varPath;
      }
      var varParts: Array<Dynamic> = parseVarPath(varPath);
      var rawTestExpr: Array<Dynamic> = varParts.slice(0);
      var getOp: Dynamic = {};
      Reflect.setField(getOp, Obfu.raw("get"), true);
      Reflect.setField(getOp, Obfu.raw("args"), varParts.length);
      var callOp: Dynamic = {};
      Reflect.setField(callOp, Obfu.raw("call"), binOp);
      Reflect.setField(callOp, Obfu.raw("args"), 2);
      testExpr = ScriptExpr.fromRaw(varParts.concat([getOp, rightExpr.asRaw(), callOp]));
    }

    var name = IF;
    var args: Map<String, ScriptExpr> = new Map();
    args.set(TEST, testExpr);
    return new ScriptNode(name, args, node.children, node.runs, node.okValue, node.key, node.onSuccess, node.clear);
  }

  private static function normalizeSetVarNode(node: ScriptNode): ScriptNode {
    Deprecated.warn("e_setVar", "Deprecated `e_setVar` action, use assignment statement");
    var varExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("var"));
    var gExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("g"));
    var valueExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("value"));
    var addExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("add"));
    var randExpr: Null<ScriptExpr> = node.args.get(Obfu.raw("rand"));

    Assert.debug(TypeUtils.isString(varExpr));
    var varPath: String = varExpr.asRaw();
    if (TypeUtils.isBool(gExpr) && gExpr.asRaw() || TypeUtils.isFloat(gExpr) && gExpr.asRaw() != 0) {
      varPath = Obfu.raw("global") + "." + varPath;
    }
    var varParts: Array<Dynamic> = parseVarPath(varPath);

    var assignmentOperator: Dynamic = true; // `=`
    var rawValue: Array<Dynamic>;
    if (valueExpr != null) {
      Assert.debug(!TypeUtils.isArray(valueExpr));
      rawValue = [valueExpr.asRaw()];
    } else if (addExpr != null) {
      Assert.debug(!TypeUtils.isArray(addExpr));
      rawValue = [Std.parseFloat(addExpr.asRaw())];
      assignmentOperator = "+"; // `+=`
    } else if (randExpr != null) {
      Assert.debug(!TypeUtils.isArray(randExpr));
      var randArg: Int = Std.parseInt(randExpr.asRaw());
      var randCall: Dynamic = {};
      Reflect.setField(randCall, Obfu.raw("call"), Obfu.raw("rand"));
      Reflect.setField(randCall, Obfu.raw("args"), 1);
      rawValue = [randArg, randCall];
    } else {
      throw new ScriptError("ScriptCompile: Missing `e_setVar` value");
    }

    var assignOp: Dynamic = {};
    Reflect.setField(assignOp, Obfu.raw("set"), assignmentOperator);
    Reflect.setField(assignOp, Obfu.raw("args"), varParts.length + 1);

    var evalExpr: ScriptExpr = ScriptExpr.fromRaw(varParts.concat(rawValue).concat([assignOp]));

    var name = EXEC;
    var args: Map<String, ScriptExpr> = new Map();
    args.set(EXPR, evalExpr);
    return new ScriptNode(name, args, node.children, node.runs, node.okValue, node.key, node.onSuccess, node.clear);
  }

  // "foo.bar[5].baz" -> ["foo", "bar", 5, "baz"]
  private static function parseVarPath(varPath: String): Array<Dynamic> {
    var result: Array<Dynamic> = [];
    for (dotPart in varPath.split(".")) {
      var bracketParts: Array<String> = dotPart.split("[");
      result.push(bracketParts[0]);
      for (bracketPart in bracketParts.slice(1)) {
        var parsed: Null<Int> = Std.parseInt(bracketPart.substr(0, bracketPart.length - 1)); // Drop `]`
        Assert.debug(parsed != null);
        result.push(parsed);
      }
    }
    return result;
  }
}
