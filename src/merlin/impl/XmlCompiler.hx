package merlin.impl;

import etwin.flash.XML;
import etwin.flash.XMLNode;
import etwin.Obfu;
import merlin.ScriptNode;

/**
 The `XmlCompiler` class allows to convert an XML string using the _classic_
 syntax to a _BetterScript_ `Script` object.
**/
class XmlCompiler {

  private static inline var REPEAT: String = Obfu.raw("repeat");
  private static inline var KEY: String = Obfu.raw("key");
  private static inline var END_CLEAR: String = Obfu.raw("endClear");
  private static inline var EXIT_CLEAR: String = Obfu.raw("exitClear");
  private static inline var ELEMENT_NODE: Int = 1;
  private static inline var TEXT_NODE: Int = 3;

  public static function compile(str: String, allowDeprecated: Bool = false): Script {
    var doc: XML = new XML(str);
    doc.ignoreWhite = true;
    var nodes: Array<ScriptNode> = compileNodeList(doc.childNodes, true, allowDeprecated);
    return new Script(nodes);
  }

  /**
   * Compiles an XML tag to the corresponding `ScriptNode` object.
   */
  private static function compileNodeList(xmlTags: Array<XMLNode>, isRoot: Bool, allowDeprecated: Bool = false): Array<ScriptNode> {
    var nodes: Array<ScriptNode> = [];
    for (child in xmlTags) {
      if (child.nodeType != ELEMENT_NODE) {
        continue;
      }
      nodes.push(compileNode(child, isRoot, allowDeprecated));
    }
    return nodes;
  }

  /**
   * Compiles an XML tag to the corresponding `ScriptNode` object.
   */
  private static function compileNode(xmlTag: XMLNode, isRoot: Bool, allowDeprecated: Bool = false): ScriptNode {
    var name: String = normalizeActionName(xmlTag.nodeName, allowDeprecated);
    var args: Map<String, ScriptExpr> = new Map();
    var children: Array<ScriptNode> = compileNodeList(xmlTag.childNodes, false, allowDeprecated);
    var runs: Float = 1;
    var okValue: Bool = true;
    var key: Null<Int> = null;
    // At the root, `detach` has no effect so we set it to `false`.
    var detach: Bool = !isRoot && children.length != 0;
    var onSuccess: NodeOnSuccess = detach ? Detach : Nothing;
    var clear: NodeClearEvent = None;

    // `Reflect.fields` returns an empty array here.
    var rawAttrNames: Array<String> = untyped __keys__(xmlTag.attributes);
    for (rawAttrName in rawAttrNames) {
      var attrName: String = normalizeArgName(rawAttrName);
      var strValue: String = Reflect.field(xmlTag.attributes, rawAttrName);
      if (strValue.length == 0) {
        continue;
      }
      var floatValue: Float = Std.parseFloat(strValue);
      var value: ScriptExpr = Math.isNaN(floatValue) ? ScriptExpr.fromString(strValue) : ScriptExpr.fromFloat(floatValue);

      switch (attrName) {
        case XmlCompiler.REPEAT:
          runs = floatValue < 0 ? Math.POSITIVE_INFINITY : floatValue;
        case XmlCompiler.KEY:
          key = Std.int(floatValue);
        case XmlCompiler.END_CLEAR:
          clear = floatValue > 0 ? End : None;
        // 1st syntax extension: add $exitClear (overriden by $endClear).
        case XmlCompiler.EXIT_CLEAR:
          if (clear == None && floatValue > 0) {
            clear = Exit;
          }
        default:
          args.set(attrName, value);
      }
    }

    // 2nd syntax extension: convert `<$e_setVar $var="foo" $value="bar"/>` into `dyn.foo = "bar"`.
    if (name == Obfu.raw("e_setVar")) {
      name = Obfu.raw("exec");
      args = compileSetVarArgs(args);
    }

    return new ScriptNode(name, args, children, runs, okValue, key, onSuccess, clear);
  }

  private static function compileSetVarArgs(args: Map<String, ScriptExpr>): Map<String, ScriptExpr> {
    var varName = args.get(Obfu.raw("var"));
    var varValue = args.get(Obfu.raw("value"));
    var varName = Std.string(varName == null ? null : varName.asRaw()[0]);
    var varValue = varValue == null ? null : varValue.asRaw()[0];
    var assignOp = {};
    Reflect.setField(assignOp, Obfu.raw("set"), true); // simple assignment
    Reflect.setField(assignOp, Obfu.raw("args"), 3);
    var expr = ScriptExpr.fromRaw([Obfu.raw("dyn"), varName, varValue, assignOp]);

    return [Obfu.raw("expr") => expr];
  }

  private static function normalizeActionName(str: String, allowDeprecated: Bool = false): String {
    return Actions.normalizeActionName(normalizeName(str), allowDeprecated);
  }

  private static function normalizeArgName(str: String): String {
    return normalizeName(str);
  }

  private static function normalizeName(str: String): String {
    if (str.charCodeAt(0) == "$".code) {
      return str.substr(1);
    } else {
      return str;
    }
  }
}
