package merlin.impl;

import etwin.ds.Nil;
import merlin.impl.bs.ScriptOpObject;
import merlin.impl.expr.BinOp;
import merlin.proxys.Scope;
import merlin.value.MerlinNull;
import merlin.value.MerlinBool;
import merlin.value.MerlinFloat;
import merlin.value.MerlinObject;
import merlin.value.MerlinString;
import merlin.value.MerlinValue;
import patchman.Assert;
import patchman.DebugConsole;

class EvalContext {
  private var stack: Array<MerlinValue>;

  private var variables: Scope;

  public static inline function eval(variables: Scope, expr: ScriptExpr): MerlinValue {
    var ctx: EvalContext = new EvalContext(variables);
    return ctx.innerEval(expr);
  }

  private function new(variables: Scope): Void {
    this.stack = [];
    this.variables = variables;
  }

  private function innerEval(expr: ScriptExpr): MerlinValue {
    if(!TypeUtils.isArray(expr))
      return expr.asRaw();

    var ops: Array<Dynamic> = expr.asRaw();

    for (op in ops) {
      if (TypeUtils.isMerlinPrimitive(op)) {
        this.stack.push(op);
      } else {
        // OK, because we can't have IProxy or ICall instances
        // in an operation sequence.
        var obj: ScriptOpObject = op;
        if (obj.call() != null) {
          this.execCall(obj.call(), obj.args());
        } else if (obj.get() != null && obj.get()) {
          this.execGet(obj.args());
        } else if (obj.set() != null) {
          var binOp = obj.set();
          if (!TypeUtils.isString(binOp)) {
            binOp = null;
          }
          this.execSet(obj.args(), binOp);
        }
      }
    }
    if (this.stack.length == 0) {
      throw new ScriptError("ExprEval: EmptyStack");
    }
    return this.stack.pop();
  }

  private inline function execCall(funcName: String, argc: Int): Void {
    this.checkAvailable(argc);
    var args = this.stack.splice(this.stack.length - argc, argc);

    var func = this.variables.get(funcName)
      .or(_ => throw new ScriptError("ExprEval: UnknownFunc: " + funcName))
      .unwrapFunction();

    var result: MerlinValue = func.call(args);
    this.stack.push(result);
  }

  private inline function execGet(argc: Int): Void {
    this.checkAvailable(argc);
    var end: Int = this.stack.length;
    var start: Int = end - argc;
    var proxy: Scope = this.variables;
    var path: Array<MerlinValue> = this.stack.slice(start, end);
    var result: MerlinValue = innerGetVar(proxy, path).or(_ => {
      DebugConsole.warn("ExprEval: UnknownVar: " + path.join("."));
      MerlinNull.NULL;
    });
    untyped this.stack.length = start;
    this.stack.push(result);
  }

  private inline function execSet(argc: Int, binOp: Null<String>): Void {
    this.checkAvailable(argc);
    if (this.stack.length == 0) {
      throw new ScriptError("ExprEval: EmptyStack");
    }
    var value: MerlinValue = this.stack.pop();
    var end: Int = this.stack.length;
    var start: Int = end - (argc - 1);
    var proxy: Scope = this.variables;
    var path: Array<MerlinValue> = this.stack.slice(start, end);
    var result: MerlinValue = innerSetVar(proxy, path, value, binOp);
    untyped this.stack.length = start;
    this.stack.push(result);
  }

  private inline function checkAvailable(argc: Int): Void {
    var availableArgs: Int = stack.length;
    if (availableArgs < argc) {
      throw new ScriptError("ExprEval: NotEnoughArguments: " + argc + " (expected), " + availableArgs + " (available)");
    }
  }

  private static inline function innerGetVar(proxy: Scope, path: Array<MerlinValue>): Nil<MerlinValue> {
    var value: Nil<MerlinValue> = Nil.none();
    var cur: MerlinObject = MerlinObject.proxy(proxy);
    for (i in 0...(path.length)) {
      var field: String = EvalContext.unwrapKey(path[i]);
      if (i + 1 == path.length) { // last
        value = cur.tryGetField(field);
      } else {
        cur = cur.getField(field).unwrapObject();
      }
    }
    return value;
  }

  /**
   * Updates the value resolved from `proxy` using `path`.
   *
   * If `binOp` is `null`, it simply sets the value:
   * ```
   * proxy[...path] = value;
   * ```
   *
   * If `binOp` is not null, the new value is updated using the operator:
   * ```
   * proxy[...path] = proxy[...path] <op> value;
   * ```
   *
   * This allows to implements update-assignment operators such as `+=`.
   */
  private static inline function innerSetVar(proxy: Scope, path: Array<MerlinValue>, value: MerlinValue, binOp: Null<String>): MerlinValue {
    var cur: MerlinObject = MerlinObject.proxy(proxy);
    for (i in 0...(path.length)) {
      var field: String = unwrapKey(path[i]);
      if (i + 1 == path.length) { // last
        if (binOp != null) {
          var oldValue: MerlinValue = cur.getField(field);
          value = callBinOp(binOp, oldValue, value);
        }
        cur.setField(field, value);
      } else {
        cur = cur.getField(field).unwrapObject();
      }
    }
    return value;
  }

  private static inline function callBinOp(op: Null<String>, left: MerlinValue, right: MerlinValue): MerlinValue {
    return switch (op) {
      case "+": Funcs.add(left, right);
      case "-": Funcs.sub(left, right);
      case "*": Funcs.mul(left, right);
      case "/": Funcs.div(left, right);
      case "%": Funcs.rem(left, right);
      default: throw new ScriptError("ExprEval: UnknownBinOp: " + op);
    }
  }

  private static inline function unwrapKey(value: MerlinValue): String {
    if (value.isFloat()) {
      return Std.string(cast value);
    } else {
      return value.unwrapString();
    }
  }
}
