package merlin.impl;

// Type-testing utilities tailored for Merlin.
// TODO: should this go in etwin-core?
class TypeUtils {

  public static inline function isBool(raw: Dynamic): Bool {
    #if (flash || js)
    return typeof(raw) == "boolean";
    #else
    return Std.is(raw, Bool);
    #end
  }

  public static inline function isFloat(raw: Dynamic): Bool {
    #if (flash || js)
    return typeof(raw) == "number";
    #else
    return Std.is(raw, Float);
    #end
  }

  public static inline function isString(raw: Dynamic): Bool {
    #if (flash || js)
    return typeof(raw) == "string";
    #else
    return Std.is(raw, String);
    #end
  }

  public static inline function isArray(raw: Dynamic): Bool {
    // More efficient (on flash) and/or correct (on js) than `Std.is(expr, Array)`.
    #if js
    return untyped Array.isArray(raw);
    #else
    return Type.getClass(raw) == Array;
    #end
  }

  public static inline function isMerlinPrimitive(raw: Dynamic): Bool {
    #if (flash || js)
    return typeof(raw) != "object";
    #else
    return raw == null || isFloat(raw) || isBool(raw) || isString(raw);
    #end 
  }

  #if flash
  private static inline function typeof(raw: Dynamic): String {
    return untyped __typeof__(raw);
  }
  #elseif js
  private static inline function typeof(raw: Dynamic): String {
    return untyped __js__("typeof")(raw);
  }
  #end
}
