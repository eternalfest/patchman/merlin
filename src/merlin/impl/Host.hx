package merlin.impl;

import hf.Case;
import hf.mode.GameMode;
import hf.levels.ScriptEngine;

typedef Host = {
  var game: GameMode;
  var bossDoorTimer: Float;
  var cycle: Float;
  var bads: Int;
  var fl_birth: Bool;
  var fl_death: Bool;
  var fl_safe: Bool;
  var fl_elevatorOpen: Bool;
  var fl_onAttach: Bool;
  var fl_firstTorch: Bool;
  var fl_redraw: Bool;
  var recentExp: Array<ScriptExp>;
  var mcList: Array<ScriptMc>;
  var entries: Array<Case>;
  function playById(id: Int): Void;
  function killById(id: Int): Void;
};
