package merlin.impl;

import hf.Hf;
import hf.mode.GameMode;
import merlin.impl.Host;
import merlin.proxys.RefMap;
import merlin.proxys.Scope;
import merlin.value.MerlinObject;
import merlin.value.MerlinValue;
import etwin.ds.FrozenMap;
import etwin.ds.WeakMap;

class Engine {
  private var config(default, null): MerlinConfig;
  private var hostScopes(default, null): WeakMap<GameMode, RefMap>;

  public function new(config: MerlinConfig) {
    this.config = config;
    this.hostScopes = new WeakMap();
  }

  /**
   * Evaluates the provided list of nodes.
   *
   * The list and its content may be mutated.
   * Returns the list of new nodes.
   */
  public function evalNodes(host: Host, nodes: Array<ScriptNode>, locals: MerlinObject): Void {
    var hostScope: RefMap = getOrCreateHostScope(host);
    var scope: Scope = new Scope(hostScope, locals);

    var newNodes: Array<ScriptNode> = [];
    var evalFunc: ScriptExpr -> MerlinValue = function(expr: ScriptExpr): MerlinValue {
      return this.evalExpr(scope, expr);
    };
    var addNode: ScriptNode -> Void = function(node: ScriptNode): Void {
      newNodes.push(node);
    };
    this.innerEvalNodes(host, evalFunc, addNode, nodes, newNodes);
    for (newNode in newNodes) {
      nodes.push(newNode);
    }
  }

  private function innerEvalNodes(
    host: Host,
    evalExpr: ScriptExpr -> MerlinValue,
    addNode: ScriptNode -> Void,
    nodes: Array<ScriptNode>,
    newNodes: Array<ScriptNode>
  ): Void {
    var i: Int = 0;
    var j: Int = 0;
    while (i < nodes.length) {
      var node: ScriptNode = nodes[i];
      nodes[j] = node;
      i++;
      if (node.runs <= 0) {
        continue;
      }

      if (node.onSuccess == Detach) {
        newNodes.push(node);
        node.onSuccess = Nothing;
      } else {
        if (node.onSuccess == Copy) {
          var clone: ScriptNode = ScriptNode.clone(node);
          clone.onSuccess = Nothing;
          newNodes.push(clone);
        }
        j++;
      }

      if (this.evalNode(host, evalExpr, addNode, node)) {
        this.innerEvalNodes(host, evalExpr, addNode, node.children, newNodes);
      }
    }

    // Keep only the first `j` items.
    untyped nodes.length = j;
  }

  private function evalNode(
    host: Host,
    evalExpr: ScriptExpr -> MerlinValue,
    addNode: ScriptNode -> Void,
    node: ScriptNode
  ): Bool {
    var action: Null<IAction> = this.getAction(node.name);
    if (action == null) {
      throw new ScriptError("UnknownAction: " + node.name);
    }

    var ctx: ActionContext = new ActionContext(host, evalExpr, addNode, node);
    var runResult: Bool = action.run(ctx);
    var result: Bool = runResult == node.okValue;

    if (result) {
      var key: Null<Int> = node.key;
      if (key != null) {
        if (ctx.hasKey(key)) {
          ctx.keyUsed(key);
        } else {
          if (action.isVerbose) {
            ctx.keyRequired(key);
          }
          return false;
        }
      }
      node.runs -= 1;
    }

    return result;
  }

  public function evalExpr(scope: Scope, expr: ScriptExpr): MerlinValue {
    return EvalContext.eval(scope, expr);
  }

  private function getAction(name: String): Null<IAction> {
    return this.config.actions.get(name);
  }

  private function getOrCreateHostScope(host: Host): RefMap {
    var game: GameMode = host.game;
    var hostScope: Null<RefMap> = hostScopes.get(game);
    if (hostScope == null) {
      hostScope = this.createHostScope(game);
      this.hostScopes.set(game, hostScope);
    }
    return hostScope;
  }

  private function createHostScope(game: GameMode): RefMap {
    var refs: Map<String, IRef> = new Map();
    for (refFactory in this.config.refs) {
      var ref: IRef = refFactory.createRef(game);
      refs.set(refFactory.name, ref);
    }
    return new RefMap(FrozenMap.uncheckedFrom(refs));
  }
}
