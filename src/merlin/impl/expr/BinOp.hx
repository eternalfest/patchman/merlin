package merlin.impl.expr;

enum BinOp {
  Add;
  Div;
  Mul;
  Rem;
  Sub;
}
