package merlin.impl.bs;

import etwin.Obfu;

// typedef Script = {
//   version: Int,
//   nodes: Array<ScriptNode>,
//   vars: Null<Dynamic>,
// }

/**
  Obfu-aware adapter for raw BetterScript `Script` objects.
**/
abstract Script({}) {
  private inline function new(fields: Dynamic) {
    this = fields;
  }

  public inline function version(): Int {
    return Obfu.field(this, "version");
  }

  public inline function setVersion(value: Int): Void {
    return Obfu.setField(this, "version", value);
  }

  public inline function nodes(): Array<ScriptNode> {
    return Obfu.field(this, "nodes");
  }

  public inline function setNodes(value: Array<ScriptNode>): Void {
    return Obfu.setField(this, "nodes", value);
  }

  public inline function vars(): Null<Dynamic> {
    return Obfu.field(this, "vars");
  }

  public inline function setVars(value: Null<Dynamic>): Void {
    return Obfu.setField(this, "vars", value);
  }
}
