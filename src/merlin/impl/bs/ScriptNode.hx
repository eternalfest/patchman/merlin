package merlin.impl.bs;

import merlin.value.MerlinValue;
import etwin.Obfu;

/**
  Obfu-aware adapter for raw BetterScript `ScriptNode` objects.
**/
abstract ScriptNode({}) {
  private inline function new(fields: Dynamic) {
    this = fields;
  }

  public static inline function make(
    name: String,
    params: Null<Dynamic>,
    children: Null<Array<ScriptNode>>,
    invert: Null<Bool>,
    detach: Null<Bool>,
    copy: Null<Bool>,
    key: Null<Int>,
    repeat: Null<Dynamic>,
    clear: Null<String>
  ): ScriptNode {
    return new ScriptNode(Obfu.raw({
      name: name,
      params: params,
      children: children,
      invert: invert,
      detach: detach,
      copy: copy,
      key: key,
      repeat: repeat,
      clear: clear,
    }));
  }

  public inline function name(): String {
    return Obfu.field(this, "name");
  }

  public inline function setName(value: String): Void {
    return Obfu.setField(this, "name", value);
  }

  public inline function params(): Null<Dynamic> {
    return Obfu.field(this, "params");
  }

  public inline function setParams(value: Null<Dynamic>): Void {
    return Obfu.setField(this, "params", value);
  }

  public inline function children(): Null<Array<ScriptNode>> {
    return Obfu.field(this, "children");
  }

  public inline function setChildren(value: Null<Array<ScriptNode>>): Void {
    return Obfu.setField(this, "children", value);
  }

  public inline function invert(): Null<Bool> {
    return Obfu.field(this, "invert");
  }

  public inline function setInvert(value: Null<Bool>): Void {
    return Obfu.setField(this, "invert", value);
  }

  public inline function detach(): Null<Bool> {
    return Obfu.field(this, "detach");
  }

  public inline function setDetach(value: Null<Bool>): Void {
    return Obfu.setField(this, "detach", value);
  }

  public inline function copy(): Null<Bool> {
    return Obfu.field(this, "copy");
  }

  public inline function setCopy(value: Null<Bool>): Void {
    return Obfu.setField(this, "copy", value);
  }

  public inline function key(): Null<Int> {
    return Obfu.field(this, "key");
  }

  public inline function setKey(value: Null<Int>): Void {
    return Obfu.setField(this, "key", value);
  }

  // `true` or `int`
  public inline function repeat(): Null<Dynamic> {
    return Obfu.field(this, "repeat");
  }

  public inline function setRepeat(value: Null<Dynamic>): Void {
    return Obfu.setField(this, "repeat", value);
  }

  public inline function clear(): Null<String> {
    return Obfu.field(this, "clear");
  }

  public inline function setClear(value: Null<String>): Void {
    return Obfu.setField(this, "clear", value);
  }
}
