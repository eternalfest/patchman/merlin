package merlin.impl.bs;

abstract ScriptExpr(Dynamic) {
  public function new(expr: Dynamic) {
    this = expr;
  }
}
