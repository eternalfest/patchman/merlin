package merlin.impl.bs;

import etwin.Obfu;

/**
  Obfu-aware adapter for raw BetterScript objects used as script ops
  (in expression evaluation).
**/
abstract ScriptOpObject({}) {
  private inline function new(fields: Dynamic) {
    this = fields;
  }

  public static inline function make(
    args: Int,
    call: Null<String>,
    get: Null<Bool>,
    set: Null<Dynamic>
  ): ScriptOpObject {
    return new ScriptOpObject({
      args: args,
      call: call,
      get: get,
      set: set,
    });
  }

  public inline function args(): Int {
    return Obfu.field(this, "args");
  }

  public inline function setArgs(value: Int): Void {
    return Obfu.setField(this, "args", value);
  }

  public inline function call(): Null<String> {
    return Obfu.field(this, "call");
  }

  public inline function setCall(value: Null<String>): Void {
    return Obfu.setField(this, "call", value);
  }

  public inline function get(): Null<Bool> {
    return Obfu.field(this, "get");
  }

  public inline function setGet(value: Null<Bool>): Void {
    return Obfu.setField(this, "get", value);
  }

  // Returns a `Bool` or `String` ("+", "-", etc.)
  public inline function set(): Null<Dynamic> {
    return Obfu.field(this, "set");
  }

  public inline function setSet(value: Null<Dynamic>): Void {
    return Obfu.setField(this, "set", value);
  }
}
