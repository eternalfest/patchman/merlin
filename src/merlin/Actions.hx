package merlin;

import merlin.actions.Add;
import merlin.actions.Attach;
import merlin.actions.Bad;
import merlin.actions.Birth;
import merlin.actions.Ctrigger;
import merlin.actions.Darkness;
import merlin.actions.Death;
import merlin.actions.Do;
import merlin.actions.End;
import merlin.actions.Enter;
import merlin.actions.Exec;
import merlin.actions.Exp;
import merlin.actions.Ext;
import merlin.actions.Fakelid;
import merlin.actions.Goto;
import merlin.actions.Hide;
import merlin.actions.HideBorders;
import merlin.actions.If;
import merlin.actions.ItemLine;
import merlin.actions.Kill;
import merlin.actions.KillMsg;
import merlin.actions.KillPt;
import merlin.actions.Mc;
import merlin.actions.Mirror;
import merlin.actions.Msg;
import merlin.actions.Multi;
import merlin.actions.Music;
import merlin.actions.Night;
import merlin.actions.Ninja;
import merlin.actions.OpenPortal;
import merlin.actions.Pmc;
import merlin.actions.Pointer;
import merlin.actions.Portal;
import merlin.actions.Pos;
import merlin.actions.Rem;
import merlin.actions.Score;
import merlin.actions.Spec;
import merlin.actions.Timer;
import merlin.actions.Tuto;
import etwin.Obfu;

/**
  The actions provided by Merlin.
**/
class Actions {
  /**
    `add` - Places tiles or fields in the level.

    @param x1: Int, y1: Int   Tile coordinates of first endpoint.
    @param x2: Int, y2: Int   Tile coordinates of second endpoint.
    @param tile: Int = -1     The id of the field to place (places tiles by default).
  **/
  public static var ADD(default, never): Add = new Add();

  /**
    `attach` - Triggers on level load.

    Doesn't take parameters.
  **/
  public static var ATTACH(default, never): Attach = new Attach();

  /**
    `bad` - Spawns an enemy.

    @param i: Int               The id of the enemy to spawn.
    @param x: Float, y: Float   Tile coordinates of the enemy.
    @param ?sid: Int            Gives the enemy a script id.
    @param sys: Bool = false    Set for enemies spawned by the level itself.
  **/
  public static var BAD(default, never): Bad = new Bad();

  /**
    `birth` - Triggers on player (re-)spawn.

    Doesn't take parameters.
  **/
  public static var BIRTH(default, never): Birth = new Birth();

  /**
    `ctrigger` - Executes a predefined action.

    @param i: 0   Warps the player to level 10 of the current dimension upon level exit.
    @param i: 1   Delays the level's "Hurry Up!" timer by a half-tick.
    @param i: 2   Hides the "Next level" arrow.
    @param i: 3   Used for the first part of level 103's animation.
    @param i: 4   Used for the second part of level 103's animation.
    @param i: 5   Used for level 101's exit door (requires Tuberculoz to be present).
    @param i: 6   Spawns a soccer ball at one of the special item slots.
    @param i: 7   Makes the player cry.
    @param i: 8   Makes the player cheer.
    @param i: 9   Plays Tuberculoz' laugh.
    @param i: 10  Prevents enemies from jumping down through tiles.
    @param i: 11  Deletes all enemies (except spikes, saws, etc).
    @param i: 12  Forces the level's fireball to spawn.
    @param i: 13  Deletes all items.
    @param i: 14  Removes all torch halos.
    @param i: 15  Resets the level's "Hurry Up!" timer.
  **/
  public static var CTRIGGER(default, never): Ctrigger = new Ctrigger();

  /**
    `darkness` - Overrides the level's darkness.

    @param v: Int   The darkness' strength, as a percentage (before reduction by quests).
  **/
  public static var DARKNESS(default, never): Darkness = new Darkness();

  /**
    `death` - Triggers on player death.

    Doesn't take parameters.
  **/
  public static var DEATH(default, never): Death = new Death();

  /**
    `do` - Triggers on each cycle.

    Doesn't take parameters.
  **/
  public static var DO(default, never): Do = new Do();

  /**
    `end` - Triggers if the level is cleared.

    Doesn't take parameters.
  **/
  public static var END(default, never): End = new End();

  /**
    `enter` - Triggers when the player enters the given tile.

    @param x: Int, y: Int   The tile's coordinates.
  **/
  public static var ENTER(default, never): Enter = new Enter();

  /**
    `exec` - Used internally by Merlin to represent statements.
  **/
  public static var EXEC(default, never): Exec = new Exec();

  /**
    `exp` - Triggers when a bomb explodes near the given tile.

    @param x: Int, y: Int   The tile's coordinates.
  **/
  public static var EXP(default, never): Exp = new Exp();

  /**
    `ext` - Used by the game to place CRISTAL letters.

    Doesn't take parameters.
  **/
  public static var EXT(default, never): Ext = new Ext();

  /**
    `fakelid` - Overrides the displayed level number.

    @param ?lid: Int  The level number to display. If not specified, uses `?` instead.
  **/
  public static var FAKELID(default, never): Fakelid = new Fakelid();

  /**
    `goto` - Forcibly warps the player to the given level.
  
    @param id: Int  The level to warp to.
  **/
  public static var GOTO(default, never): Goto = new Goto();

  /**
    `hide` - Changes how level's tiles and borders are shown.

    @param tiles: Bool = false    Should the tiles be hidden?
    @param borders: Bool = false  Should the borders be hidden?
  **/
  public static var HIDE(default, never): Hide = new Hide();

  /**
    `hideBorders` - Hides the level's borders, without affecting tiles.

    Doesn't take parameters.
  **/
  public static var HIDE_BORDERS(default, never): HideBorders = new HideBorders();

  /**
    `if` - Triggers if the expression evaluates to `true`.

    @param <anon>: Bool   The expression to test.
  **/
  public static var IF(default, never): If = new If();

  /**
    `itemLine` - Spawns several score items in a line.

    Unlike `score`, the items are permanent and do not despawn.

    @param i: Int             The id of the score items to spawn.
    @param ?si: Int           The sub-id of the score items to spawn.
    @param x1: Int, x2: Int   The `x` coordinates of the line's endpoints.
    @param y: Int             The `y` coordinate of the line.
    @param t: Int             The delay between two consecutive items, in cycles.
  **/
  public static var ITEM_LINE(default, never): ItemLine = new ItemLine();

  /**
    `kill` - Removes a previously placed sprite, item, or enemy.

    @param sid: Int   The script id of the sprite or entity to remove. 
  **/
  public static var KILL(default, never): Kill = new Kill();

  /**
    `killMsg` - Removes any message scroll currently on-screen.

    Doesn't take parameters.
  **/
  public static var KILL_MSG(default, never): KillMsg = new KillMsg();
  
  /**
    `killPt` - Removes any pointer arrow currently on-screen.

    Doesn't take parameters.
  **/
  public static var KILL_PT(default, never): KillPt = new KillPt();


  /**
    `mc` - Places decorative sprites.

    @param x/xr: Float, x/yr: Float   The sprite's coordinates (in tiles for `x/y`, in pixels for `xr/yr`).
    @param n: String            The name of the sprite to place.
    @param back: Bool = false   Should the sprite appear behind entities?
    @param p: Bool = false      Should the sprite play its animation?
    @param flip: Bool = false   Should the sprite be flipped horizontally?
    @param ?sid: Int            Gives the sprite a script id.
  **/
  public static var MC(default, never): Mc = new Mc();

  /**
    `mirror` - Triggers if the *Mirror* option is activated.

    Doesn't take parameters.
  **/
  public static var MIRROR(default, never): Mirror = new Mirror();

  /**
    `msg` - Shows a message scroll.

    @param id: Int    The id of the message to display.
  **/
  public static var MSG(default, never): Msg = new Msg();
  
  /**
    `multi` - Triggers if multiple players are present.

    Doesn't take parameters.
  **/
  public static var MULTI(default, never): Multi = new Multi();

  /**
    `music` - Changes the music that is currently playing.

    @param id: Int    The id of the music to play.
  **/
  public static var MUSIC(default, never): Music = new Music();

  /**
    `mirror` - Triggers if the *Nightmare* option is activated.

    Doesn't take parameters.
  **/
  public static var NIGHT(default, never): Night = new Night();

  /**
    `mirror` - Triggers if the *Ninjutsu* option is activated.

    Doesn't take parameters.
  **/
  public static var NINJA(default, never): Ninja = new Ninja();

  /**
    `openPortal` - Open a blue vortex in the level.

    @param x: Int, y: Int   Tile coordinates of the vortex.
    @param pid: Int         The portal id of the vortex.
  **/
  public static var OPEN_PORTAL(default, never): OpenPortal = new OpenPortal();

  /**
    `pmc` - Plays the animation of the given sprite.

    @param sid: Int         The script id of the sprite to target.
  **/
  public static var PMC(default, never): Pmc = new Pmc();

  /**
    `pointer` - Shows an arrow pointer to the given location.

    @param x: Float, y: Float   Tile coordinates to point to.
  **/
  public static var POINTER(default, never): Pointer = new Pointer();

  /**
    `portal` - Warps the player by taking a portal link.

    The player will only be warped is the level is cleared.

    @param pid: Int   The portal id to use.
  **/
  public static var PORTAL(default, never): Portal = new Portal();

  /**
    `pos` - Triggers if the player is in a circular region.

    @param x: Float, y: Float   Tile coordinates of the center of the region.
    @param d: Float             Radius of the region, in tiles (diameter is `2d+1`).
  **/
  public static var POS(default, never): Pos = new Pos();

  /**
    `rem` - Remove tiles or fields in the level.

    @param x1: Int, y1: Int   Tile coordinates of first endpoint.
    @param x2: Int, y2: Int   Tile coordinates of second endpoint.
  **/
  public static var REM(default, never): Rem = new Rem();

  /**
    `score` - Spawns a score item.

    @param i: Int               The id of the score item to spawn.
    @param ?si: Int             The sub-id of the score item to spawn.
    @param x: Float, y: Float   Tiles coordinates of item.
    @param inf: Bool = false    Should the item never despawn?
    @param ?sid: Int            Gives the item a script id.
  **/
  public static var SCORE(default, never): Score = new Score();

  /**
    `score` - Spawns a special item.

    @param i: Int               The id of the special item to spawn.
    @param ?si: Int             The sub-id of the special item to spawn.
    @param x: Float, y: Float   Tiles coordinates of item.
    @param inf: Bool = false    Prevents the item from despawning.
    @param force: Bool = false  Force the item to spawn, even if the level is already cleared.
    @param ?sid: Int            Gives the item a script id.
  **/
  public static var SPEC(default, never): Spec = new Spec();

  /**
    `timer` - Triggers after a certain amount of time.

    @param t: Int             Time before the first activation, in cycles.
    @param ?base: Int         Time before subsequent activations, in cycles (equal to `t` if not specified).
    @param rel: Bool = false  Make the timer relative.
  **/
  public static var TIMER(default, never): Timer = new Timer();

  /**
    `msg` - Shows a tutorial message scroll.

    @param id: Int    The id of the message to display.
  **/
  public static var TUTO(default, never): Tuto = new Tuto();

  private static var NORMALIZED_NAMES(default, never): Map<String, String> = [
    Obfu.raw("e_add") => ADD.name,
    // Obfu.raw("attach") => ATTACH.name,
    Obfu.raw("e_bad") => BAD.name,
    // Obfu.raw("birth") => BIRTH.name,
    Obfu.raw("e_ctrigger") => CTRIGGER.name,
    Obfu.raw("e_darkness") => DARKNESS.name,
    // Obfu.raw("death") => DEATH.name,
    // Obfu.raw("do") => DO.name,
    // Obfu.raw("end") => END.name,
    // Obfu.raw("enter") => ENTER.name,
    // Obfu.raw("exec") => EXEC.name,
    // Obfu.raw("exp") => EXP.name,
    Obfu.raw("e_ext") => EXT.name,
    Obfu.raw("e_fakelid") => FAKELID.name,
    Obfu.raw("e_goto") => GOTO.name,
    Obfu.raw("e_hide") => HIDE.name,
    Obfu.raw("e_hideBorders") => HIDE_BORDERS.name,
    // Obfu.raw("if") => IF.name,
    Obfu.raw("e_itemLine") => ITEM_LINE.name,
    Obfu.raw("e_kill") => KILL.name,
    Obfu.raw("e_killMsg") => KILL_MSG.name,
    Obfu.raw("e_killPt") => KILL_PT.name,
    Obfu.raw("e_mc") => MC.name,
    // Obfu.raw("mirror") => MIRROR.name,
    Obfu.raw("e_msg") => MSG.name,
    // Obfu.raw("multi") => MULTI.name,
    Obfu.raw("e_music") => MUSIC.name,
    // Obfu.raw("night") => NIGHT.name,
    // Obfu.raw("ninja") => NINJA.name,
    Obfu.raw("e_openPortal") => OPEN_PORTAL.name,
    Obfu.raw("e_pmc") => PMC.name,
    Obfu.raw("e_pointer") => POINTER.name,
    Obfu.raw("e_portal") => PORTAL.name,
    Obfu.raw("t_pos") => POS.name,
    Obfu.raw("e_rem") => REM.name,
    Obfu.raw("e_score") => SCORE.name,
    Obfu.raw("e_spec") => SPEC.name,
    Obfu.raw("t_timer") => TIMER.name,
    Obfu.raw("e_tuto") => TUTO.name
  ];

  public static function get(): Map<String, IAction> {
    var actions: Map<String, IAction> = new Map();
    actions.set(ADD.name, ADD);
    actions.set(ATTACH.name, ATTACH);
    actions.set(BAD.name, BAD);
    actions.set(BIRTH.name, BIRTH);
    actions.set(CTRIGGER.name, CTRIGGER);
    actions.set(DARKNESS.name, DARKNESS);
    actions.set(DEATH.name, DEATH);
    actions.set(DO.name, DO);
    actions.set(END.name, END);
    actions.set(ENTER.name, ENTER);
    actions.set(EXEC.name, EXEC);
    actions.set(EXP.name, EXP);
    actions.set(EXT.name, EXT);
    actions.set(FAKELID.name, FAKELID);
    actions.set(GOTO.name, GOTO);
    actions.set(HIDE.name, HIDE);
    actions.set(HIDE_BORDERS.name, HIDE_BORDERS);
    actions.set(IF.name, IF);
    actions.set(ITEM_LINE.name, ITEM_LINE);
    actions.set(KILL.name, KILL);
    actions.set(KILL_MSG.name, KILL_MSG);
    actions.set(KILL_PT.name, KILL_PT);
    actions.set(MC.name, MC);
    actions.set(MIRROR.name, MIRROR);
    actions.set(MSG.name, MSG);
    actions.set(MULTI.name, MULTI);
    actions.set(MUSIC.name, MUSIC);
    actions.set(NIGHT.name, NIGHT);
    actions.set(NINJA.name, NINJA);
    actions.set(OPEN_PORTAL.name, OPEN_PORTAL);
    actions.set(PMC.name, PMC);
    actions.set(POINTER.name, POINTER);
    actions.set(PORTAL.name, PORTAL);
    actions.set(POS.name, POS);
    actions.set(REM.name, REM);
    actions.set(SCORE.name, SCORE);
    actions.set(SPEC.name, SPEC);
    actions.set(TIMER.name, TIMER);
    actions.set(TUTO.name, TUTO);
    return actions;
  }

  public static function normalizeActionName(name: String, allowDeprecated: Bool = false): String {
    var normalized: Null<String> = NORMALIZED_NAMES.get(name);
    // TODO: Disable check in release mode
    if (normalized != null) {
      if (!allowDeprecated) {
        Deprecated.warn("deprecatedName", "Deprecated action name " + name + ", use " + normalized);
      }
      name = normalized;
    }
    return name;
  }
}
