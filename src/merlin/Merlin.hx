package merlin;

import etwin.ds.Nil;
import hf.mode.GameMode;
import merlin.value.MerlinValue;
import merlin.impl.Engine;
import patchman.IPatch;
/**
  The minimal script engine for Hammerfest!
**/
@:build(patchman.Build.di())
class Merlin {
  private var config: MerlinConfig;

  @:diExport
  public var patch(default, null): IPatch;

  public function new(config: MerlinConfig) {
    var privateConfig: MerlinConfig = config.clone();
    var engine = new Engine(config);
    this.patch = ScriptPatch.create(engine);
  }

  public static function getGlobalVar(game: GameMode, name: String): Nil<MerlinValue> {
    return MerlinState.getGlobalObject(game).tryGetField(name);
  }

  public static function setGlobalVar(game: GameMode, name: String, value: MerlinValue): Void {
    MerlinState.getGlobalObject(game).setField(name, value);
  }

  public static function getLevelVar(level: hf.levels.Data, name: String): Nil<MerlinValue> {
    return MerlinState.getLevelState(level).object.tryGetField(name);
  }

  public static function setLevelVar(level: hf.levels.Data, name: String, value: MerlinValue): Void {
    return MerlinState.getLevelState(level).object.setField(name, value);
  }

  /**
  Resolves the value of `level.<name>` for the current level.
  */
  public static function getCurLevelVar(game: GameMode, name: String): Nil<MerlinValue> {
    return MerlinState.getCurLevelState(game).object.tryGetField(name);
  }

  public static function setCurLevelVar(game: GameMode, name: String, value: MerlinValue): Void {
    return MerlinState.getCurLevelState(game).object.setField(name, value);
  }

  public static function getLevelVarAt(game: GameMode, dimId: Int, levelId: Int, name: String): Nil<MerlinValue> {
    return MerlinState.getLevelStateAt(game, dimId, levelId).object.tryGetField(name);
  }

  public static function setLevelVarAt(game: GameMode, dimId: Int, levelId: Int, name: String, value: MerlinValue): Void {
    return MerlinState.getLevelStateAt(game, dimId, levelId).object.setField(name, value);
  }
}
