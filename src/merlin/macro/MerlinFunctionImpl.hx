package merlin.macro;

#if macro
import haxe.macro.Expr;
import haxe.macro.Context;

class MerlinFunctionImpl {
  public static function fixedArity(func: Expr): Expr {
    var pos = func.pos;
    var argLen = switch (func.expr) {
      // Don't type function literals to avoid issues with inference.
      case EFunction(_, f): f.args.length;
      default: switch (Context.typeof(func)) {
        case TFun(args, ret): args.length;
        default: Context.error("`MerlinFunction.fixedArity`: expected a function", pos);
      }
    };

    // Check the type of the function.
    var merlinValTy = macro: merlin.value.MerlinValue;
    var funcTy = TFunction([for (_ in 0...argLen) merlinValTy], merlinValTy);
    var func = { expr: ECheckType(func, funcTy), pos: pos };

    // Emit macro output.
    var argsName = "args$"; // Unnameable var, for hygiene.
    var errMsg = 'InvalidArgCount: expected $argLen, got ';
    var argList = [for (i in 0...argLen) {
      macro @:pos(pos) $i{argsName}[$v{i}];
    }];

    return macro @:pos(pos) merlin.value.MerlinFunction.fromFunc(
      function($argsName) {
        if ($i{argsName}.length == $v{argLen}) {
          return $func($a{argList});
        } else {
          throw new merlin.ScriptError($v{errMsg} + $i{argsName}.length);
        }
      }
    );
  }
}
#end
