package merlin;

import merlin.value.MerlinValue;

/**
 Represents a callable Merlin function.
**/
interface ICall {
  /**
   Call the function with the given arguments.
  **/
  public function call(args: Array<MerlinValue>): MerlinValue;
}
