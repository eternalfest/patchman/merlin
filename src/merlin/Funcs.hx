package merlin;

import merlin.value.MerlinValue;

class Funcs {

  public static inline function add(left: MerlinValue, right: MerlinValue): MerlinValue {
    if (left.isString() && right.isString()) {
      return (left.unwrapString(): String) + (right.unwrapString(): String);
    }
    if (left.isFloat() && right.isFloat()) {
      return left.unwrapFloat().toFloat() + right.unwrapFloat().toFloat();
    }
    throw new ScriptError("TypeError");
  }
  
  public static inline function and(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapBool().toBool() && right.unwrapBool().toBool();
  }

  public static inline function bitAnd(left: MerlinValue, right: MerlinValue): MerlinValue {
    return ((cast left.unwrapFloat()): Int) & ((cast right.unwrapFloat()): Int);
  }

  public static inline function bitNot(arg: MerlinValue): MerlinValue {
    return ~((cast arg.unwrapFloat()): Int);
  }
  
  public static inline function bitOr(left: MerlinValue, right: MerlinValue): MerlinValue {
    return ((cast left.unwrapFloat()): Int) | ((cast right.unwrapFloat()): Int);
  }
  
  public static inline function bitXor(left: MerlinValue, right: MerlinValue): MerlinValue {
    return ((cast left.unwrapFloat()): Int) ^ ((cast right.unwrapFloat()): Int);
  }

  public static inline function div(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapFloat().toFloat() / right.unwrapFloat().toFloat();
  }

  public static inline function eq(left: MerlinValue, right: MerlinValue): MerlinValue {
    return (left: Dynamic) == (right: Dynamic);
  }

  public static inline function gt(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapFloat().toFloat() > right.unwrapFloat().toFloat();
  }
  
  public static inline function gte(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapFloat().toFloat() >= right.unwrapFloat().toFloat();
  }
  
  public static inline function lt(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapFloat().toFloat() < right.unwrapFloat().toFloat();
  }
  
  public static inline function lte(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapFloat().toFloat() <= right.unwrapFloat().toFloat();
  }

  public static function max(args: Array<MerlinValue>): MerlinValue {
    var result: Float = Math.NEGATIVE_INFINITY;
    for (arg in args) {
      var f: Float = arg.unwrapFloat();
      if (f != null && f > result) {
        result = f;
      }
    }
    return result;
  }
  
  public static function min(args: Array<MerlinValue>): MerlinValue {
    var result: Float = Math.POSITIVE_INFINITY;
    for (arg in args) {
      var f: Float = arg.unwrapFloat();
      if (f != null && f < result) {
        result = f;
      }
    }
    return result;
  }
  
  public static inline function mul(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapFloat().toFloat() * right.unwrapFloat().toFloat();
  }
  
  public static inline function ne(left: MerlinValue, right: MerlinValue): MerlinValue {
    return ((cast left): Dynamic) != ((cast right): Dynamic);
  }
  
  public static inline function not(arg: MerlinValue): MerlinValue {
    return !arg.unwrapBool().toBool();
  }
  
  public static inline function or(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapBool() || right.unwrapBool();
  }
  
  public static inline function rem(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapFloat().toFloat() % right.unwrapFloat().toFloat();
  }
  
  public static inline function sub(left: MerlinValue, right: MerlinValue): MerlinValue {
    return left.unwrapFloat().toFloat() - right.unwrapFloat().toFloat();
  }
}
