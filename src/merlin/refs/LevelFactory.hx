package merlin.refs;

import hf.mode.GameMode;
import merlin.value.MerlinObject;
import merlin.value.MerlinValue;
import etwin.Obfu;

class LevelFactory implements IRefFactory {
  public var name(default, null): String;

  public function new(name: String) {
    this.name = name;
  }

  public function createRef(game: GameMode): IRef {
    return new Level(game);
  }
}

class Level implements IRef {
  private var game: GameMode;

  public function new(game: GameMode) {
    this.game = game;
  }

  public function get(): MerlinValue {
    return MerlinState.getCurLevelState(this.game).object;
  }

  public function set(value: MerlinValue): Bool {
    return false;
  }
}
