package merlin.refs;

import hf.mode.GameMode;
import merlin.value.MerlinObject;

class GlobalFactory implements IRefFactory {
  public var name(default, null): String;

  public function new(name: String) {
    this.name = name;
  }

  public function createRef(game: GameMode): IRef {
    return new FrozenRef(MerlinState.getGlobalObject(game));
  }
}
