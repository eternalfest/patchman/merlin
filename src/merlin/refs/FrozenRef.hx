package merlin.refs;

import merlin.value.MerlinValue;

class FrozenRef implements IRef {
  private var val: MerlinValue;

  public inline function new(val: MerlinValue) {
    this.val = val;
  }

  public inline function get(): MerlinValue {
    return this.val;
  }

  public inline function set(value: MerlinValue): Bool {
    return false;
  }
}
