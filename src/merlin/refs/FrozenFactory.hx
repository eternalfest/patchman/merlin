package merlin.refs;

import hf.mode.GameMode;
import merlin.value.MerlinValue;

class FrozenFactory implements IRefFactory {
  public var name(default, null): String;
  private var value: MerlinValue;

  public function new(name: String, value: MerlinValue) {
    this.name = name;
    this.value = value;
  }

  public function createRef(game: GameMode): IRef {
    return new FrozenRef(this.value);
  }
}

