package merlin;

import merlin.value.MerlinValue;
import etwin.ds.Nil;

/**
  Interface describing the implementation of a merlin object.
**/
interface IProxy {
  /**
  Get the value of the field `key`

  Returns `Nil.NONE` if the field is missing.
  -*/
  public function get(key: String): Nil<MerlinValue>;

  /**
  Create or update the field `key`
  -*/
  public function set(key: String, value: MerlinValue): Void;

  /**
  Check if the field `key` exists.

  The following property must hold:
  ```
  proxy.has(cx, key) == proxy.get(cx, key).isSome()
  ```
  -*/
  public function has(key: String): Bool;
}
