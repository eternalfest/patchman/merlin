package merlin;

import merlin.value.MerlinValue;

/**
  Represents a value reference.

  Allows to manually handle `set/get` operations.
  For example, it can be used to implement `NO_NEXT_LEVEL`.
**/
interface IRef {
  /**
    Get the current value of the reference
  **/
  function get(): MerlinValue;

  /**
   Set a new value for the reference

   @returns Boolean indicating if the value was updated. (`false` indicates
   an error such as readonly or invalid fields)
  **/
  function set(value: MerlinValue): Bool;
}
