package merlin.proxys;

import etwin.ds.Nil;
import merlin.value.MerlinValue;

class Plain implements IProxy {
  public var fields(default, null): Map<String, MerlinValue>;

  public function new() {
    this.fields = new Map();
  }

  public inline function get(key: String): Nil<MerlinValue> {
    if (this.fields.exists(key)) {
      return Nil.some(this.fields.get(key));
    } else {
      return Nil.none();
    }
  }

  public inline function set(key: String, value: MerlinValue): Void {
    this.fields.set(key, value);
  }

  public inline function has(key: String): Bool {
    return this.fields.exists(key);
  }
}
