package merlin.proxys;

import etwin.ds.Nil;
import hf.mode.GameMode;
import merlin.value.MerlinValue;
import etwin.Obfu;

class Dyn implements IProxy {
  public var game(default, null): GameMode;

  public function new(game: GameMode) {
    this.game = game;
  }

  public function get(key: String): Nil<MerlinValue> {
    // `dvars` forces lower case
    if (!this.game.dvars.exists(key.toLowerCase())) {
      return Nil.none();
    }
    // Prefix with `$` because `getDynamicVar` removes the first character
    var gameValue: Dynamic = this.game.getDynamicVar(Obfu.raw("$") + key);
    if (!MerlinValue.test(gameValue)) {
      throw new ScriptError('dynamic var $key is not a MerlinValue');
    }
    return Nil.some(gameValue);
  }

  public inline function set(key: String, value: MerlinValue): Void {
    this.game.setDynamicVar(key, value);
  }

  public inline function has(key: String): Bool {
    // `dvars` forces lower case
    return this.game.dvars.exists(key.toLowerCase());
  }
}
