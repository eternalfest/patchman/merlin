package merlin.proxys;

import etwin.ds.FrozenMap;
import etwin.ds.Nil;
import etwin.Error;
import merlin.value.MerlinObject;
import merlin.value.MerlinValue;

/**
  Proxy for the scope of a script

  When scripts access a variable, it is first checked in the host-provided
  variables, and only then as a local level variable. It means that level
  variable can never shadow host variables. If you need a level variable with
  the same name as a host variable, use the explicit `level.foo` syntax instead
  of only `foo`.
**/
class Scope implements IProxy {
  public var hostVars(default, null): RefMap;
  public var levelVars(default, null): MerlinObject;

  public function new(hostVars: RefMap, levelVars: MerlinObject) {
    this.hostVars = hostVars;
    this.levelVars = levelVars;
  }

  public inline function get(key: String): Nil<MerlinValue> {
    return this.hostVars.get(key).or(_ => this.levelVars.tryGetField(key));
  }

  public inline function set(key: String, value: MerlinValue): Void {
    if (this.hostVars.has(key)) {
      // This will check again for the existence of `key`.
      // Solving this would require to add an `.update(key, value): Bool` method
      // to proxies.
      this.hostVars.set(key, value);
    } else {
      this.levelVars.setField(key, value);
    }
  }

  public inline function has(key: String): Bool {
    return this.hostVars.has(key) || this.levelVars.hasField(key);
  }
}
