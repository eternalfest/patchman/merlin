package merlin.proxys;

import etwin.ds.FrozenMap;
import etwin.ds.Nil;
import merlin.value.MerlinValue;

class RefMap implements IProxy {
  public var refs(default, null): FrozenMap<String, IRef>;

  public function new(refs: FrozenMap<String, IRef>) {
    this.refs = refs;
  }

  public inline function get(key: String): Nil<MerlinValue> {
    if (this.refs.exists(key)) {
      var ref: IRef = this.refs.get(key);
      return Nil.some(ref.get());
    } else {
      return Nil.none();
    }
  }

  public inline function set(key: String, value: MerlinValue): Void {
    if (this.refs.exists(key)) {
      var ref: IRef = this.refs.get(key);
      ref.set(value);
    } else {
      throw new ScriptError("ReferenceNotFound: " + key);
    }
  }

  public inline function has(key: String): Bool {
    return this.refs.exists(key);
  }
}
