package merlin.proxys;

import etwin.ds.Nil;
import hf.mode.GameMode;
import merlin.value.MerlinValue;
import patchman.Assert;
import etwin.Obfu;

class Callable implements ICall {
  #if !(flash8 || js)
  private var func: Array<MerlinValue> -> MerlinValue;
  #end

  public function new(func: Array<MerlinValue> -> MerlinValue) {
    // We can avoid an indirection by replacing the `call` method
    // by the function we want to call.
    // This is only done on targets where this is legal.
    #if (flash8 || js)
    untyped this.call = func;
    #else
    this.func = func;
    #end
  }

  public function call(args: Array<MerlinValue>): MerlinValue {
    #if (flash8 || js)
    return null;
    #else
    return this.func(args);
    #end
  }
}
