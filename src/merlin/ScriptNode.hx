package merlin;

@:enum
abstract NodeClearEvent(Int) {
  var None = 0;
  var End = 1;
  var Exit = 2;
}

@:enum
abstract NodeOnSuccess(Int) {
  var Nothing = 0;
  var Detach = 1;
  var Copy = 2;
}

/**
  Runtime representation of a script node.
**/
class ScriptNode {
  /**
    Name of the node.

    The name is used as the key identifying the action to run for this node.
  **/
  public var name(default, null): String;

  /**
    Map from argument names to their expressions.

    The argument expresssions are evaluated every time the argument is
    retrieved.
    The `args` map may change at runtime but its expressions are treated as
    immutable.
  **/
  public var args(default, null): Map<String, ScriptExpr>;

  /**
    Child nodes to run when this node is validated.
  **/
  public var children(default, null): Array<ScriptNode>;

  /**
    Number of `run` calls remaining before removing this node.

    This count is only decreased when `run` returns true.
    If the value is `0`, the node is removed instead of being executed.
    The value is a positive integer or `+Infinity`.

    This field is used to implement the `repeat` modifier.
  **/
  public var runs: Float;

  /**
    If not `None`, disable this node after the corresponding event.
  **/
  public var clear(default, null): NodeClearEvent;

  /**
    Value expected from `run` to consume the node.

    The `run` call is validated (decrement `runs`, run children) if it returns
    `okValue`.
    This field is used to implement the `invert` modifier.
  **/
  public var okValue(default, null): Bool;

  /**
    Key id.

    If `key` is non-null, the action is only executed if the user has the key
    with the corresponding id.
    If the action has its `isVerbose` flag set, a message will be displayed on
    the HUD to notify the player that a key is required or used.
  **/
  public var key(default, null): Null<Int>;

  /**
    What to do with this node when it succeeds.

    Used for implemented `detach` and `copy` modifiers.
  **/
  public var onSuccess: NodeOnSuccess;

  public function new(
    name: String,
    args: Map<String, ScriptExpr>,
    children: Array<ScriptNode>,
    runs: Float = 1,
    okValue: Bool = true,
    ?key: Int,
    onSuccess: NodeOnSuccess = Nothing,
    clear: NodeClearEvent = None
  ): Void {
    this.name = name;
    this.args = args;
    this.children = children;
    this.runs = runs;
    this.okValue = okValue;
    this.key = key;
    this.onSuccess = onSuccess;
    this.clear = clear;
  }

  public function traverse(fn: ScriptNode -> Void): Void {
    fn(this);
    for (child in this.children) {
      child.traverse(fn);
    }
  }

  public static function clone(value: ScriptNode): ScriptNode {
    return new ScriptNode(
      value.name,
      ScriptNode.cloneArgs(value.args),
      value.children.map(ScriptNode.clone),
      value.runs,
      value.okValue,
      value.key,
      value.onSuccess,
      value.clear
    );
  }

  private static inline function cloneArgs(value: Map<String, ScriptExpr>): Map<String, ScriptExpr> {
    var result: Map<String, ScriptExpr> = new Map();
    for (name in value.keys()) {
      // We are assuming that the expression are immutable so it safe to not clone them.
      result.set(name, value.get(name));
    }
    return result;
  }
}
