package merlin;

import patchman.Assert;
import patchman.DebugConsole;
import etwin.Obfu;
import etwin.ds.WeakMap;
import hf.mode.GameMode;
import merlin.impl.BsCompiler;
import merlin.impl.XmlCompiler;
import merlin.value.MerlinObject;

class MerlinState {

  private static var GLOBALS: WeakMap<GameMode, MerlinObject> = new WeakMap();

  private static var STATES: WeakMap<hf.levels.Data, MerlinState> = new WeakMap();

  /**
    Sentinel value indicating a compiled script.
    It verifies `new XML(str).toString() == str`, because the scriptEngine will serialize
    its XML back into the level when `GameMechanics.suspend` is called.
  **/
  private static inline var COMPILED_MERLIN_SCRIPT: String = Obfu.raw("<merlin />");

  public var object(default, null): MerlinObject;
  public var script(default, default): Null<Script>;

  public function new() {
    this.object = MerlinObject.plain();
    this.script = null;
  }

  public static function getGlobalObject(game: GameMode): MerlinObject {
    var global: Null<MerlinObject> = GLOBALS.get(game);
    if (global == null) {
      global = MerlinObject.plain();
      GLOBALS.set(game, global);
    }
    return global;
  }

  public static function getLevelState(level: hf.levels.Data): MerlinState {
    Assert.debug(level != null);
    var state: Null<MerlinState> = STATES.get(level);
    if (state == null) {
      state = new MerlinState();
      STATES.set(level, state);
    }
    return state;
  }

  public static function getLevelStateAt(game: GameMode, dimId: Int, levelId: Int): MerlinState {
    var world = game.dimensions[dimId];
    if (world != null && levelId >= 0 && levelId < world.levels.length) {
      if (!world.fl_read[levelId]) {
        // The level was never read, so unserialize it.
        world.levels[levelId] = world.unserialize(levelId);
      }

      return getLevelState(world.levels[levelId]);
    }

    throw new etwin.Error('MerlinStateError: invalid level did=$dimId; lid=$levelId');
  }

  public static inline function getCurLevelState(game: GameMode): MerlinState {
    return getLevelState(game.world.current);
  }

  public static function compileLevelScript(level: hf.levels.Data): MerlinState {
    var state = getLevelState(level);

    // There may be three possible values for `level.__dollar__script`
    // - `COMPILED_MERLIN_SCRIPT`: Special sentinel value indicating the script
    //   is already compiled
    // - Better-Script node: An object-based encoding of the level script
    // - Serialized XML document: Original format
    var inputScript: Dynamic = level.__dollar__script;
    if (inputScript == COMPILED_MERLIN_SCRIPT) {
      Assert.debug(state.script != null, "Missing compiled level script");
      return state;
    }
    
    // Compile the script and store it.
    state.script = if (inputScript == "" || inputScript == null) {
      new Script();
    } else if (Std.is(inputScript, String)) {
      DebugConsole.warn("Deprecated XML script found, please upgrade it to better-scripts");
      XmlCompiler.compile(inputScript, true);
    } else {
      // Assuming it is a BetterScript node object
      BsCompiler.compile(inputScript);
    };

    // Put our sentinel value to indicate that the script is compiled.
    level.__dollar__script = COMPILED_MERLIN_SCRIPT;
    return state;
  }
}
