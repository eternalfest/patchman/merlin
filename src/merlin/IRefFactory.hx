package merlin;

import hf.mode.GameMode;

interface IRefFactory {
  var name(default, null): String;

  function createRef(game: GameMode): IRef;
}
