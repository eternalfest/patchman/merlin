package merlin;

import patchman.Assert;
import etwin.ds.WeakMap;
import hf.GameManager;
import hf.Hf;
import hf.levels.GameMechanics;
import hf.levels.ScriptEngine;
import hf.mode.GameMode;
import merlin.impl.Engine;
import merlin.impl.Host;
import merlin.impl.XmlCompiler;
import merlin.proxys.RefMap;
import merlin.ScriptNode.NodeClearEvent;
import patchman.DebugConsole;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

/**
 Class handling the creation of the patch.
**/
class ScriptPatch {

  public static function create(engine: Engine): IPatch {
    var hostScopes: WeakMap<GameMode, RefMap> = new WeakMap();

    return new PatchList([
      Ref.auto(ScriptEngine.runScript).replace(function(hf: Hf, self: ScriptEngine): Void {
        return ScriptPatch.runScript(self, engine, hostScopes);
      }),
      Ref.auto(ScriptEngine.addScript).replace(function(hf: Hf, self: ScriptEngine, script: String): Void {
        return ScriptPatch.addScript(self, script);
      }),
      Ref.auto(GameMechanics.onDataReady).before(function(hf: Hf, self: GameMechanics): Void {
        // Add support for `clear=exit`
        ScriptPatch.clearNodes(self.current, Exit);
      }),
      Ref.auto(ScriptEngine.compile).replace(function(hf: Hf, self: ScriptEngine): Void {
        return ScriptPatch.compile(self);
      }),
      Ref.auto(ScriptEngine.clearScript).replace(function(hf: Hf, self: ScriptEngine): Void {
        return ScriptPatch.clearScript(self);
      }),
      Ref.auto(ScriptEngine.clearEndTriggers).replace(function(hf: Hf, self: ScriptEngine): Void {
        // Add support for `clear=end`
        ScriptPatch.clearNodes(self.data, End);
      })
    ]);
  }

  private static function compile(scriptEngine: ScriptEngine): Void {
    scriptEngine.history = [];

    // We are skipping the `traceHistory` part from the original function
    // since it is long and does not add much value.

    // Compile the level script, if required.
    var state = MerlinState.compileLevelScript(scriptEngine.data);

    // The game engine may add some extra scripts during the level initialization (to support bads, items or extends)
    // using the `.addScript` method.
    // These extra scripts are currently handled using the original XML format.
    // They are only intended for compatibility with the scripts used by the game engine
    // (custom actions are not supported in the extra scripts).
    var extraInputScript: String = scriptEngine.extraScript;
    if (extraInputScript != "") {
      var extraScript: Script = XmlCompiler.compile(extraInputScript, true);
      for (node in extraScript.nodes) {
        state.script.nodes.push(node);
      }
    }

    // Finalize the compilation:
    // 1. Ensure the sentinel value is applied
    scriptEngine.baseScript = scriptEngine.data.__dollar__script;
    var xmlGuard = new etwin.flash.XML(scriptEngine.baseScript);
    xmlGuard.ignoreWhite = true;
    scriptEngine.script = xmlGuard;
    // 2. Ensure the script engine state is properly updated
    scriptEngine.normalMode();
    scriptEngine.fl_compile = true;
    // 3. Run the first cycle
    scriptEngine.traceHistory("first=" + scriptEngine.cycle);
    scriptEngine.runScript();
  }

  private static function addScript(scriptEngine: ScriptEngine, script: String): Void {
    if (scriptEngine.fl_compile) {
      var newScript: Script = XmlCompiler.compile(script, true);
      var compiledScript: Null<Script> = MerlinState.getLevelState(scriptEngine.data).script;
      if (compiledScript == null) {
        // TODO: Throw
        DebugConsole.error("ScriptError: `.fl_compile` but missing compiled script.");
        return;
      }
      for (newNode in newScript.nodes) {
        compiledScript.nodes.push(newNode);
      }
    } else {
      scriptEngine.extraScript += " " + script;
    }
    // Skip history tracing
  }

  private static function clearScript(scriptEngine: ScriptEngine): Void {
    // The only difference here is the extra line to clear the compiled merlin script
    MerlinState.getLevelState(scriptEngine.data).script = null;
    scriptEngine.script = null;
    scriptEngine.baseScript = "";
    scriptEngine.extraScript = "";
    scriptEngine.cycle = 0;
    scriptEngine.fl_compile = false;
  }

  private static function clearNodes(level: hf.levels.Data, clearEvent: NodeClearEvent): Void {
    var script: Script = MerlinState.getLevelState(level).script;
    if (script == null) {
      // The script is not compiled yet.
      return;
    }

    script.traverse(function(node: ScriptNode) {
      if (node.clear == clearEvent) {
        node.runs = 0;
      }
    });
  }

  private static function runScript(scriptEngine: ScriptEngine, engine: Engine, hostScopes: WeakMap<GameMode, RefMap>): Void {
    var state: MerlinState = MerlinState.getLevelState(scriptEngine.data);
    var script: Null<Script> = state.script;
    if (script == null) {
      // The script is not compiled yet.
      return;
    }

    try {
      engine.evalNodes(scriptEngine, script.nodes, state.object);
    } catch (err: ScriptError) {
      DebugConsole.error("ScriptError: " + err.message);
    }

    scriptEngine.fl_birth = false;
    scriptEngine.fl_death = false;
    scriptEngine.fl_onAttach = false;
    scriptEngine.recentExp = [];
    scriptEngine.entries = [];
  }
}
