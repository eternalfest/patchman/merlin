# 0.17.1 (2023-01-05)

- **[Doc]** Document all actions and references.

# 0.17.0 (2022-12-21)

- **[Breaking change]** Make `ClearEvent` into a tri-state enum:
  - it is renamed to to `ScriptNode.NodeClearEvent`;
  - the possible values are `None`, `End`, `Exit`; `null` is no longer valid.
- **[Breaking change]** Replace `ScriptNode.{detach, copy}` flags by a single `ScriptNode.NodeOnSuccess` enum:
  - the possible values are `Nothing`, `Detach`, `Copy`.
- **[Breaking change]** Make `ScriptExpr` into an abstract type.
- **[Breaking change]** Replace `MerlinFunction.{fixedArity1, fixedArity2}` by the `fixedArity` macro,
  which accepts functions with an arbitrary number of arguments.
- **[Feature]** Add conversions from `Float/Bool/String` to `MerlinValue`.
- **[Feature]** Add `IActionContext.getOptString` method.
- **[Feature]** `mc` now supports mixed coordinates; e.g. `mc(n=..., x=12.5, yr=148)`.
- **[Change]** Make arguments to `ScriptNode`'s constructor optional (except for `name`, `args`, `children`).
- **[Change]** Move internal classes to `merlin.impl`.
- **[Change]** Remove `IHost` and `IActionContext` interfaces (`IActionContext` is kept as a typedef
  for backwards compatibility)
- **[Fix]** Fix `ScriptExpr.fromValue` for `MerlinObject`s and `MerlinFunction`s.
- **[Fix]** Errors thrown when executing scripts are now properly caught and don't crash the whole game.
- **[Fix]** Fix `mc` miscentering torch halos when the sprite is mirrored.
- **[Fix]** Do not show required key messages for non-verbose actions.
- **[Internal]** Slightly improve performances by using dedicated type tests instead of `Std.is`.

# 0.16.0 (2021-04-21)

- **[Breaking change]** Update to `@patchman@0.10.4`.
- **[Internal]** Update to Yarn 2.

# 0.15.0 (2021-01-15)
- **[Breaking change]** Changes to `MerlinState` API:
  - Merge `MerlinState` and `LevelState` classes.
  - Remove `MerlinState.getLevelObject` methods (use `getLevelState(...).object` instead).
  - `MerlinState.getLevelState` and `Merlin.{get, set}LevelVar` now take a `hf.levels.Data` arguments.
  - Use `MerlinState.getLevelStateAt` and `Merlin.{get, set}LevelVarAt` for accessing state by level id.
- **[Feature]** Add `MerlinState.compileLevelScript` method for manually compiling a script.
- **[Feature]** Add `IActionContext.getArgNames` method.
- **[Fix]** Level scripts are properly saved when exiting a level normally.
- **[Fix]** Correctly apply extra scripts instead of crashing when warping into a level already visited.

# 0.14.3 (2020-12-19)
- **[Fix]** Fix script operators `<`, `>`, and `>=`, `<=`, being reversed.

# 0.14.2 (2020-12-04)
- **[Fix]** Fix `$exitClear` attribute not actually working.

# 0.14.1 (2020-12-04)

- **[Feature]** Implement ctrigger id=6 (`attachSoccerBall`).
- **[Feature]** Add `$exitClear` attribute to XML scripts.
- **[Fix]** Don't treat empty scripts as XML scripts.

# 0.14.0 (2020-11-29)

- **[Breaking change]** Replace `ScriptValue` with `MerlinValue`.
  - There are 7 kinds of values: `MerlinBool`, `MerlinFloat`, `MerlinFunction`,
    `MerlinNull`, `MerlinObject` and `MerlinString`.
  - `MerlinObject`s can be created from instances of `IProxy`.
  - `MerlinFunction`s can be created from instances of `ICall`.
- **[Breaking change]** Refactor variable resolution system.
  - Three built-in scopes: `global`, `level` (the default), `dyn`.
  - Remove `IFunc` interface; variables and functions are now registered through `IRef` and `IRefFactory`.
  - Add convenience classes `FrozenRef` and `FrozenFactory`.
  - Allow access to `level` variables through `MerlinState.getLevelObject`.
- **[Breaking change]** Variables `AQUA`, `BAD_BOMB_TRIGGER`, `ICE`, `SAW_SPEED`, `SPEAR_SKIN` are no longer special-cased.
  - They now are in the `dyn` scope and must be accessed as such: `dyn.AQUA`, etc...
  - They do not persist when exiting and reentering a level.
- **[Feature]** Add `max` function.
- **[Feature]** Add legacy `e_setVar` support for XML scripts, by mapping it to `dyn` variable access.
- **[Change]** Introduce and use `ScriptError` class for errors thrown by script eval or compilation.
- **[Change]** Accessing a non-existing variable no longer throws; a warning is displayed instead and `null` is returned.
- **[Fix]** Update from `patchman.ds` to `ef.ds`.
- **[Fix]** Fix wrong torch positioning when using `mc`. 
- **[Fix]** Fix `itemLine` action without `si` causing an `EmptyStack` error.
- **[Fix]** Make `lid` optional for `fakelid`, causing the level id to be displayed as `?`.
- **[Fix]** Fix `add` actions executing only once.
- **[Fix]** Fix support for restoring scripts when returning to a level.
- **[Fix]** Fix `key` node modifier displaying the key required message on each frame.
- **[Internal]** Remove extra `hf` explicit arguments when already passing `game`.
- **[Internal]** Add tests.

# 0.13.0 (2020-09-02)

- **[Breaking change]** Update to `patchman@0.9.0`.

# 0.12.3 (2020-08-27)

- **[Feature]** Add support for `clear="end"` and `clear="exit"` (and `$endClear` in XML scripts).
- **[Fix]** Correctly destroy entities with same `sid` when using `bad` action.

# 0.12.2 (2020-02-28)

- **[Fix]** Add `flip` support for `mc`.

# 0.12.1 (2020-02-18)

- **[Fix]** Make `type` optional for `add`.

# 0.12.0 (2020-01-08)

- **[Breaking change]** Update to `patchman@0.8.0`.

# 0.11.1 (2020-01-07)

- **[Fix]** Update dependencies.
- **[Fix]** Remove deprecated use of `patchman.Hf`.
- **[Fix]** Remove deprecated use of `Ref.get` and `Ref.call`.

# 0.11.0 (2019-12-10)

- **[Breaking change]** Update to `patchman@0.7.1`.
- **[Fix]** Allow `openPortal` to use float coordinates.

# 0.10.1 (2019-11-04)

- **[Fix]** Remove deprecated name warnings for extra scripts.

# 0.10.0 (2019-11-01)

- **[Breaking change]** `Merlin` no longer implements `IPatchProvider`; patches are exported with `@:diExport` instead.
- **[Breaking change]** Remove `IXXXProvider` interface; use `@:diExport` instead.
- **[Breaking change]** Update to `patchman@0.6.2`.
- **[Fix]** Events aren't incorrectly removed after a single execution.

# 0.9.0 (2019-09-19)

- **[Breaking change]** Update to `patchman@0.5.0`.

# 0.8.0 (2019-09-19)

- **[Breaking change]** Allow proxies for non-namespaced variables.
- **[Breaking change]** Remove `dvars` proxy.
- **[Fix]** Remove custom `Merlin` factory.

# 0.7.1 (2019-09-16)

- **[Fix]** Add support for positional `if`.
- **[Fix]** Add support for deprecated `e_setVar`.
- **[Fix]** Add support for deprecated `ifVar`.
- **[Fix]** Add support for deprecated legacy `dvars` access.
- **[Fix]** Add deprecation warnings.

# 0.7.0 (2019-09-16)

- **[Breaking change]** Update to `patchman@0.4.0`.

# 0.6.0 (2019-09-16)

- **[Breaking change]** Update to `patchman@0.3.0`.

# 0.5.1 (2019-09-07)

- **[Fix]** Fix provider support.
- **[Fix]** Update to `patchman@0.2.2`.

# 0.5.0 (2019-09-06)

- **[Breaking change]** Update to `patchman@0.2.0`.

# 0.4.1 (2019-09-04)

- **[Feature]** Add `dvars` proxy.

# 0.4.0 (2019-09-03)

- **[Breaking change]** Update to `@patchman/patchman` version 0.1.0.

# 0.3.0 (2019-08-26)

- **[Breaking change]** Rename to `merlin`.
- **[Feature]** Add support for expressions.
- **[Feature]** Add `If` and `Exec` actions.

# 0.2.0 (2019-08-26)

- **[Breaking change]** Rename to `scripto`.
- **[Breaking change]** Use different `ScriptNode` representation for runtime and AST.

# 0.1.1 (2019-08-24)

- **[Fix]** Make it actually work.
