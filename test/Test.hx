package;

import etwin.Error;

class Test {
  public var name(default, null): String;
  private var fn: Void -> Void;

  public function new(name: String, fn: Void -> Void) {
    this.name = name;
    this.fn = fn;
  }

  public function run(): TestResult {
    try {
      this.fn();
      return TestResult.Ok;
    } catch (err: Error) {
      return TestResult.Error(err);
    }
  }
}
