package;

class TestSuite {
  public var tests(default, null): Array<Test>;

  public function new() {
    this.tests = [];
  }

  public function addTest(name: String, fn: Void -> Void): Void {
    this.tests.push(new Test(name, fn));
  }

  public function run(): Report {
    var results: Array<TestResult> = [];
    for (test in this.tests) {
      results.push(test.run());
    }
    return new Report(results);
  }
}
