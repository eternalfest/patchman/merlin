package actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import merlin.value.MerlinValue;
import etwin.Obfu;

class Log implements IAction {
  public var name(default, null): String = Obfu.raw("log");
  public var isVerbose(default, null): Bool = false;

  private var logs(default, null): Array<MerlinValue>;

  public function new(logs: Array<MerlinValue>) {
    this.logs = logs;
  }

  public function run(ctx: IActionContext): Bool {
    var msg: MerlinValue = ctx.getArg(Obfu.raw("msg"));
    this.logs.push(msg);
    return true;
  }
}
