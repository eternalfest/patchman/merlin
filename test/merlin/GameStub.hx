package merlin;

import hf.Hf;

class GameStub {
  public var root: Hf;

  public function new() {
    this.root = cast new HfStub();
  }
}
