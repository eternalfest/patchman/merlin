package merlin;

import merlin.value.MerlinFloat;
import merlin.value.MerlinBool;
import etwin.Error;
import merlin.value.MerlinValue;
import merlin.value.MerlinString;

class MerlinValueTest {
  public static function register(suite: TestSuite) {
    suite.addTest("testMerlinBoolConstructor", testMerlinBoolConstructor);
    suite.addTest("testMerlinBoolConversion", testMerlinBoolConversion);
    suite.addTest("testMerlinFloatConstructor", testMerlinFloatConstructor);
    suite.addTest("testMerlinFloatConversion", testMerlinFloatConversion);
    suite.addTest("testMerlinStringConstructor", testMerlinStringConstructor);
    suite.addTest("testMerlinStringConversion", testMerlinStringConversion);
  }

  public static function testMerlinBoolConstructor() {
    var bool: MerlinBool = new MerlinBool(true);
    var val: MerlinValue = bool.toMerlinValue();
    if (!val.isBool()) {
      throw new Error("Expected `.isBool` to be true");
    }
    var unwrapped: MerlinBool = val.unwrapBool();
    var raw: Bool = unwrapped;
    if (raw != true) {
      throw new Error("Expected raw value to be `true`");
    }
  }

  public static function testMerlinBoolConversion() {
    var bool: MerlinBool = true;
    var val: MerlinValue = bool.toMerlinValue();
    if (!val.isBool()) {
      throw new Error("Expected `.isBool` to be true");
    }
    var unwrapped: MerlinBool = val.unwrapBool();
    var raw: Bool = unwrapped;
    if (raw != true) {
      throw new Error("Expected raw value to be `true`");
    }
  }

  public static function testMerlinFloatConstructor() {
    var num: MerlinFloat = new MerlinFloat(3.14);
    var val: MerlinValue = num.toMerlinValue();
    if (!val.isFloat()) {
      throw new Error("Expected `.isFloat` to be true");
    }
    var unwrapped: MerlinFloat = val.unwrapFloat();
    var raw: Float = unwrapped;
    if (raw != 3.14) {
      throw new Error("Expected raw value to be `3.14`");
    }
  }

  public static function testMerlinFloatConversion() {
    var num: MerlinFloat = 3.14;
    var val: MerlinValue = num.toMerlinValue();
    if (!val.isFloat()) {
      throw new Error("Expected `.isFloat` to be true");
    }
    var unwrapped: MerlinFloat = val.unwrapFloat();
    var raw: Float = unwrapped;
    if (raw != 3.14) {
      throw new Error("Expected raw value to be `3.14`");
    }
  }

  public static function testMerlinStringConstructor() {
    var str: MerlinString = new MerlinString("foo");
    var val: MerlinValue = str.toMerlinValue();
    if (!val.isString()) {
      throw new Error("Expected `.isString` to be true");
    }
    var unwrapped: MerlinString = val.unwrapString();
    var raw: String = unwrapped;
    if (raw != "foo") {
      throw new Error("Expected raw value to be `\"foo\"`");
    }
  }

  public static function testMerlinStringConversion() {
    var str: MerlinString = "foo";
    var val: MerlinValue = str.toMerlinValue();
    if (!val.isString()) {
      throw new Error("Expected `.isString` to be true");
    }
    var unwrapped: MerlinString = val.unwrapString();
    var raw: String = unwrapped;
    if (raw != "foo") {
      throw new Error("Expected raw value to be `\"foo\"`");
    }
  }
}
