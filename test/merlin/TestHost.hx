package merlin;

import hf.Hf;
import etwin.flash.MovieClip;
import hf.Case;
import etwin.Error;
import hf.levels.ScriptEngine.ScriptExp;
import hf.levels.ScriptEngine.ScriptMc;
import hf.mode.GameMode;

class TestHost {
  public var game: GameMode;
  public var bossDoorTimer: Float;
  public var cycle: Float;
  public var bads: Int;
  public var fl_birth: Bool;
  public var fl_death: Bool;
  public var fl_safe: Bool;
  public var fl_elevatorOpen: Bool;
  public var fl_onAttach: Bool;
  public var fl_firstTorch: Bool;
  public var fl_redraw: Bool;
  public var recentExp: Array<ScriptExp>;
  public var mcList: Array<Dynamic>;
  public var entries: Array<Case>;

  public function new() {
    this.game = cast new GameStub();
    this.bossDoorTimer = 32 * 1.2;
    this.cycle = 0;
    this.bads = 0;
    this.fl_birth = false;
    this.fl_death = false;
    this.fl_safe = false;
    this.fl_elevatorOpen = false;
    this.fl_onAttach = false;
    this.fl_firstTorch = false;
    this.fl_redraw = false;
    this.recentExp = [];
    this.mcList = [];
    this.entries = [];
  }

  public function playById(id: Int): Void {}
  public function killById(id: Int): Void {}
}
