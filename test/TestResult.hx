package;

import etwin.Error;

enum TestResult {
  Ok;
  Skip;
  Error(err: Error);
}
