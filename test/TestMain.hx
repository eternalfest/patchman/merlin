package;

import actions.Log;
import etwin.ds.WeakMap;
import haxe.ds.StringMap;
import merlin.impl.BsCompiler;
import merlin.impl.bs.Script in BsScript;
import merlin.impl.Engine;
import merlin.MerlinConfig;
import merlin.MerlinValueTest;
import merlin.Script;
import merlin.TestHost;
import merlin.value.MerlinObject;
import merlin.value.MerlinValue;
import merlin.Refs;
import haxe.Json;

class TestMain {
  static function main() {
    var suite: TestSuite = new TestSuite();
    MerlinValueTest.register(suite);
    for (testItem in TestMain.getTestItems()) {
      suite.addTest(testItem.name, function() {
        var bsScriptJson: String = sys.io.File.getContent(haxe.io.Path.join([testItem.dir, "main.json"]));
        var bsScript: BsScript = Json.parse(bsScriptJson);
        if (bsScript.version() != 1) {
          throw new etwin.Error("Expected version to be `1`");
        }
        var host: TestHost = new TestHost();
        var script: Script = BsCompiler.compile(/*host, */bsScript, false);
        var config: MerlinConfig = MerlinConfig.empty();
        var logs: Array<MerlinValue> = [];
        config.addAction(new Log(logs));
        config.addRef(Refs.ADD);
        var engine: Engine = new Engine(config);
        engine.evalNodes(host, script.nodes, MerlinObject.plain());
      });
    }
    var report = suite.run();
    report.write(Sys.stdout());
    if (report.errorCount > 0) {
      Sys.exit(1);
    }
  }

  static function getTestItems(): Array<{
    name: String, dir: String
  }> {
    var testItems: Array<{
      name: String, dir: String
    }> = [];

    var testResourcesDir: String = haxe.io.Path.join([Sys.getCwd(), "test-resources"]);

    for (ent in sys.FileSystem.readDirectory(testResourcesDir)) {
      if (ent.indexOf(".") == 0) {
        continue;
      }
      var entPath: String = haxe.io.Path.join([testResourcesDir, ent]);
      if (!sys.FileSystem.isDirectory(entPath)) {
        continue;
      }
      testItems.push({name: ent, dir: entPath});
    }

    return testItems;
  }
}
