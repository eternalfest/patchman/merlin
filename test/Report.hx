package;

import etwin.Error;
import haxe.io.Output;

class Report {
  public var count(default, null): Int;
  public var okCount(default, null): Int;
  public var errorCount(default, null): Int;
  public var skipCount(default, null): Int;
  private var errors(default, null): Map<String, Error>;

  public function new(results: Array<TestResult>) {
    var count = results.length;
    var okCount = 0;
    var errorCount: Int = 0;
    var skipCount = 0;
    var errors: Map<String, Error> = new Map();
    for (r in results) {
      switch (r) {
        case TestResult.Ok:
          okCount += 1;
        case TestResult.Error(e):
          errorCount += 1;
          errors.set(Std.string(errorCount), e);
        case TestResult.Skip:
          skipCount += 1;
      }
    }
    this.count = count;
    this.okCount = okCount;
    this.errorCount = errorCount;
    this.skipCount = skipCount;
    this.errors = errors;
  }

  public function write(w: Output) {
    w.writeString("Tests: " + this.count + "\n");
    w.writeString("Ok: " + this.okCount + "\n");
    w.writeString("Error: " + this.errorCount + "\n");
    w.writeString("Skip: " + this.skipCount + "\n");
    for (k in this.errors.keys()) {
      w.writeString(k + ": " + this.errors.get(k).toString() + "\n");
    }
    w.flush();
  }
}
