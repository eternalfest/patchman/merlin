import {parse} from "@eternalfest/better-scripts";
import fs from "fs";
import url from "url";
import sysPath from "path";

const TEST_RESOURCES = sysPath.join(url.fileURLToPath(import.meta.url), "..", "test-resources");

async function compile() {
  for (const ent of fs.readdirSync(TEST_RESOURCES, {withFileTypes: true})) {
    if (ent.isDirectory() && !ent.name.startsWith(".")) {
      const source = fs.readFileSync(sysPath.join(TEST_RESOURCES, ent.name, "main.script"), {encoding: "utf-8"});
      const obj = parse(source).script.export();
      fs.writeFileSync(
        sysPath.join(TEST_RESOURCES, ent.name, "main.json"),
        `${JSON.stringify(obj, null, 2)}\n`,
        {encoding: "utf-8"},
      );
    }
  }
}

compile();
