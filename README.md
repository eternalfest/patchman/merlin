# @patchman/merlin

Minimal Haxe script engine for Hammerfest.

## Usage

```haxe
// Create a new config object, containing the base scripts
var config: MerlinConfig = MerlinConfig.base();
// Add your custom actions and references:
config.addAction(myAction);
config.addRef(myRef);
// Generate a Merlin instance using the config
var merlin: Merlin = new Merlin(config);
// Apply the patches
Patchman.patchAll(merlin.getPatches(), hf);
```

## Provided objects

Merlin provides some actions, variables and functions by default.
See the documentation of `merlin.Actions` and `merlin.Refs` for the full list.
